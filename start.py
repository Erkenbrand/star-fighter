#!/usr/bin/env python3

"""Startet das Spiel."""

import pygame
import pygame.freetype

pygame.mixer.pre_init(44100, -16, 2, 2048)

pygame.init()
pygame.mixer.set_num_channels(128)
import pyworld
from menu import Menu
from config import settings

if __name__ == '__main__':

    fenster = pyworld.World("Starfighter", settings["resolution"],
                            settings["flags"])
    fenster.set_scene(Menu())
    fenster.main()
