
import os
from random import randint
import math

import pygame

import pyogi
from pyogi.locals import *

from config import DEFAULTFONT, settings
import menu
from ship import Ship
import pyworld
import vector
import image
import effects
import objects
import items
import enemy
import backgrund

import cProfile

class Game(pyworld.Scene):
    """Basis des spiels kontrolliert die gesamte logic."""

    def __init__(self, level, player):
        """Basis des spiels kontrolliert die gesamte logic.

        level : eine Level object das fuer die Generierung der Gegner und
                Gegenstaende zustaendig ist.
        player : ein Spieler object der die Informationen wie das Inventar des
                 Spielenden beinhaltet.
        """
        pyworld.Scene.__init__(self, settings["fps"])
        if settings["music"]:
            pygame.mixer.music.load(
                os.path.join("daten", "Musik",
                             "battle3" + ".ogg"))
            pygame.mixer.music.play(-1)
        elif not settings["music"]:
            pygame.mixer.music.stop()
        cursor_image = ("          xxx           ",
                        "       xxx x xxx        ",
                        "     xx    x    xx      ",
                        "    x      x      x     ",
                        "   x       x       x    ",
                        "  x        x        x   ",
                        "  x        x        x   ",
                        " x         x         x  ",
                        " x         x         x  ",
                        " x         x         x  ",
                        "x          x          x ",
                        "xxxxxxxxxxxxxxxxxxxxxxx ",
                        "x          x          x ",
                        " x         x         x  ",
                        " x         x         x  ",
                        " x         x         x  ",
                        "  x        x        x   ",
                        "  x        x        x   ",
                        "   x       x       x    ",
                        "    x      x      x     ",
                        "     xx    x    xx      ",
                        "       xxx x xxx        ",
                        "          xxx           ",
                        "                        ")
        datatuple, masktuple = pygame.cursors.compile(cursor_image,
                                                      black='.', white='x',
                                                      xor='o')
        pygame.mouse.set_cursor((24, 24), (12, 12), datatuple, masktuple)

        # spiel Daten
        # spierit Gruppen des spiels
        self.alle = pygame.sprite.Group()
        self.particle = pygame.sprite.Group()
        self.truemmer = pygame.sprite.Group()
        self.wall = pygame.sprite.Group()
        self.energie = pygame.sprite.Group()
        self.geld = pygame.sprite.Group()
        self.feinde = pygame.sprite.Group()
        self.geschosse = pygame.sprite.Group()
        self.feind_geschosse = pygame.sprite.Group()
        self.backgrunds = pygame.sprite.Group()###

        # das Level wird geladen
        self.live_field = pygame.Rect(-settings["resolution"][0]/2,
                                      -settings["resolution"][1],
                                      settings["resolution"][0]*2,
                                      settings["resolution"][1]*3)
        self.player = player
        player.set_game(self)
        self.alle.add(player.ship)
        player.ship.set_pos((0, settings["resolution"][1]/2), "midleft")

        self.level = level(self, settings["resolution"])

        # pyogi wird eingerichtet
        self.gui = pyogi.Gui(settings["resolution"])

        self.energie_anzeige = pyogi.XStatusBar(
            self.player.ship.max_energie, widget_bg=(100, 0, 0),
            text_fg=(255, 255, 255), font=DEFAULTFONT)
        self.geld_anzeige = pyogi.Text("0", font=DEFAULTFONT,
                                       text_fg=(255, 255, 0))
        self.schild_anzeige = pyogi.XStatusBar(
            self.player.ship.max_schild, widget_bg=(0, 0, 150),
            widget_fg=(0, 0, 255), text_fg=(255, 255, 255), font=DEFAULTFONT)
        self.gui.place(self.geld_anzeige, (0, 0), anchor="topright",
                       side="topright")
        self.gui.place(self.energie_anzeige, (0, 0), anchor="topleft",
                       side="topleft")
        pyogi.place.to(self.energie_anzeige, self.schild_anzeige, "topleft",
                       "topright", (20, 0))
        self.key_pressed = 0
        self.energie_anzeige.set(self.player.ship.energie)

    def set_ob_pos(self, ob, pos):
        """Setzt die position eines neu erstellten objects."""######
        if pos == "random":
            ob.set_pos((self.screen_array.w + ob.rect.w,
                        randint(0, self.screen_array.h)))
            ob.set_rotation(math.radians(randint(160, 200)))
        else:
            ob.set_pos(pos)

    def add_drum(self, dater, pos="random"):
        """Fuegt ein neues object der Klasse Truemmer zum Spiel hinzu."""
        self.set_ob_pos(objects.Truemmer(self, (self.alle, self.truemmer),
                                         dater), pos)

    def add_wall(self, dater, pos="random"):
        """Fuegt ein neues object der Klasse Wall zum Spiel hinzu."""
        self.set_ob_pos(objects.Wall(self, (self.alle, self.wall), dater), pos)

    def add_item(self, dater, pos="random"):
        """Fuegt ein neues object der Klasse Item zum Spiel hinzu."""
        if dater["typ"] == "Geld":
            self.set_ob_pos(items.Item(self, (self.alle, self.geld), dater),
                            pos)
        elif dater["typ"] == "energie":
            self.set_ob_pos(items.Item(self, (self.alle, self.energie), dater),
                            pos)

    def add_enemy(self, dater, pos="random"):
        """Fuegt ein neues object der Klasse Feind zum Spiel hinzu."""
        feind = enemy.Feind(self, (self.alle, self.feinde), dater)
        self.set_ob_pos(feind, pos)

    def add_backgrund(self, dater, pos="random"):
        """Fuegt ein neues object der Klasse Backgrund zum Spiel hinzu."""
        self.set_ob_pos(backgrund.Backgrund(self, (self.backgrunds, ),
                                            dater), pos)

    def set_backgrund(self, backgrund):
        """Legt den Hintergrund des Spiels fest."""
        self.backgrund = backgrund

    def update(self, past_time):
        """Aktualisiert das Spiel testet auf Nutzer eingaben."""
        self.alle.update(past_time)
        self.level.update(past_time)
        self.backgrunds.update(past_time)
        player = self.player
        keys_pressed = pygame.key.get_pressed()

        #  Nutzer Eingabe des Spielers
        if keys_pressed[K_a]:
            player.move_left(past_time)
        elif keys_pressed[K_d]:
            player.move_right(past_time)
        if keys_pressed[K_w]:
            player.move_up(past_time)
        elif keys_pressed[K_s]:
            player.move_down(past_time)
        if pygame.mouse.get_pressed()[0]:
            self.player.feuer(vector.Dot(*pygame.mouse.get_pos()))

        self.test_collision()

        # anzeigen der gui updaten
        self.gui.update()
        self.schild_anzeige.set(self.player.ship.schild)
        if self.player.ship.energie <= 0:
            self.end_game()
            ship = self.player.ship
            explosion = effects.Effect((self.alle,), ship.rect.center,
                                       ship.explosion_image, 100)
            return

    def test_collision(self):
        """Prueft alles Kollisionen zueinander."""
        ship = self.player.ship
        for geld in pygame.sprite.spritecollide(ship, self.geld, True):
            self.player.geld += geld.wert
            self.geld_anzeige.text = str(self.player.geld)
            self.geld_anzeige.set_side(self.gui.get_side("topright"),
                                       side="topright")
        for energie in pygame.sprite.spritecollide(ship, self.energie, True):
            ship.energie += energie.wert
            self.energie_anzeige.set(self.player.ship.energie)

        for wall in pygame.sprite.spritecollide(ship, self.wall, False):
            if wall.energie:
                ship.energie -= wall.energie
                wall.energie = 0
            else:
                ship.energie -= ship.energie
            self.energie_anzeige.set(self.player.ship.energie)

        for drum in pygame.sprite.spritecollide(ship, self.truemmer, False):
            ship.energie -= drum.energie
            self.energie_anzeige.set(self.player.ship.energie)
            drum.energie = 0
        for feind in pygame.sprite.spritecollide(ship, self.feinde, False):
            ship.energie -= feind.energie
            self.energie_anzeige.set(self.player.ship.energie)
            feind.energie = 0
        for schuss in pygame.sprite.spritecollide(ship, self.feind_geschosse,
                                                  True):
            ship.treffer(-schuss.schaden, self.alle)
            self.energie_anzeige.set(self.player.ship.energie)
        for drum in self.truemmer:
            for geschoss in self.geschosse:
                if geschoss.rect.colliderect(drum.rect):
                    geschoss.kill()
                    effects.Effect((self.alle, ), geschoss.rect.center,
                                   geschoss.explosion, 50)
                    drum.energie -= geschoss.schaden
                    break
        for feind in self.feinde:
            for geschoss in self.geschosse:
                if geschoss.rect.colliderect(feind.rect):
                    geschoss.kill()
                    effects.Effect((self.alle,), geschoss.rect.center,
                                   geschoss.explosion, 50)
                    feind.energie -= geschoss.schaden
                    break
        for wall in self.wall:
            for geschoss in self.geschosse:
                if geschoss.rect.colliderect(wall.rect):
                    geschoss.kill()
                    effects.Effect((self.alle, ), geschoss.rect.center,
                                   geschoss.explosion, 50)
                    wall.energie -= geschoss.schaden
                    break

    def end_game(self):
        """Beendet das Spiel und geht zurueck ins Menue."""
        self.gui.clean()
        image.clean()
        self.new_scene(menu.Menu())

    def draw(self, image):
        """Zeichnet alles."""
        image.fill(0)
        self.backgrunds.draw(image)
        self.backgrund.draw(image)
        self.alle.draw(image)
        self.gui.draw(image)

    def key_down(self, event):
        if event.key == K_ESCAPE:
            self.master.set_scene(menu.Menu())

    def event(self, event):
        self.gui.test_event(event)

    def mouse_up(self, event):
        if event.button == 4:
            self.player.next_weapon()
        elif event.button == 5:
            self.player.previous_weapon()
