"""Classen um die Bewegung und Drehung von Objekten einfacher zu machen."""

import math


# IDEA: Vielleicht komplexe Zahlen benutzen.

class Dot:

    def __init__(self, x, y):
        """Repraesentiert Eine einfache Koordinate."""
        self.x = x
        self.y = y

    @property
    def pos(self):
        return (self.x, self.y)

    @pos.setter
    def pos(self, value):
        self.x = value[0]
        self.y = value[1]

    def __getitem__(self, idx):
        if not idx:
            return self.x
        elif idx == 1:
            return self.y
        else:
            raise IndexError("only 0 (x) and 1 (y) idx allowed.")

    def __setitem__(self, idx, value):
        if not idx:
            self.x += value
        elif idx == 1:
            self.y += value
        else:
            raise IndexError("only 0 (x) and 1 (y) idx allowed.")

    def __iter__(self):
        return iter((self.x, self.y))

    def __len__(self):
        return 2

    def __add__(self, dot):
        return Dot(self.x + dot[0], self.y + dot[1])

    def vector(self, dot):
        """Gibt ein object Vektor aus zwei Dots zurück."""
        return Vector(dot.x - self.x, dot.y - self.y)

    def __repr__(self):
        return f"{self.__class__, self.x, self.y}"


class Rdot(Dot):

    def __init__(self, x, y, radians=0):
        """Repraesentiert Eine einfache Koordinate, mit Drehung."""
        Dot.__init__(self, x, y)
        self._radians = radians

    def rotate(self, radians):
        self._radians += radians

    def move(self, length):
        """Bewegt den punkt Richtung Drehung um die Laenge length."""
        self.x += math.cos(self._radians) * length
        self.y += math.sin(self._radians) * length

    def move_offset(self, length, offset):
        """Wie move aber addiert offset(x, y) hinzu."""
        self.x += math.cos(self._radians) * length + offset[0]
        self.y += math.sin(self._radians) * length + offset[1]

    @property
    def radians(self):
        return self._radians

    @radians.setter
    def radians(self, radians):
        self._radians = radians

    @property
    def degrees(self):
        return math.degrees(self._radians)

    @degrees.setter
    def degrees(self, degrees):
        self._radians = math.radians(degrees)

    def __add__(self, rdot):
        return Rdot(self.x + rdot[0], self.y + rdot[1],
                    self._radians + rdot[2])

    def __getitem__(self, idx):
        if not idx:
            return self.x
        elif idx == 1:
            return self.y
        elif idx == 2:
            return self._radians
        else:
            raise IndexError("only 3 idx allowed.")

    def __setitem__(self, idx, value):
        if not idx:
            self.x = value
        elif idx == 1:
            self.y = value
        elif idx == 2:
            self._radians = value
        else:
            raise IndexError("only 3 idx allowed.")

    def __iter__(self):
        return iter((self.x, self.y, self._radians))

    def __len__(self):
        return 3


class Vector(Dot):

    def __add__(self, vector):
        return Vector(self.x + vector[0], self.y + vector[1])

    def __sub__(self, vector):
        return Vector(self.x - vector[0], self.y - vector[1])

    @property
    def v(self):
        """Gibt den vector zurueck (Hypotenuse)."""
        return math.hypot(self.x, self.y)

    def rotate(self, radians):
        sin = math.sin(radians)
        cos = math.cos(radians)
        self.x = self.x * cos - self.y * sin
        self.y = self.x * sin + self.y * cos

    @property
    def radians(self):
        return math.atan2(self.y, self.x)

    @property
    def degrees(self):
        return math.degrees(self.radians)

    def tuple_rotation(self):
        v = self.v
        try:
            xv = self.x / v
        except ZeroDivisionError:
            xv = 0
        try:
            yv = self.y / v
        except ZeroDivisionError:
            yv = 0
        return xv, yv

    def get_tuple(self):
        return self.x, self.y, self.radians


def difference(p1, p2):
    return p2.x - p1.x, p2.y - p1.y


def radian(p1, p2):
    diff = difference(p1, p2)
    return math.atan2(diff[1], diff[0])


def rotate(x, y, radians):
    sin = math.sin(radians)
    cos = math.cos(radians)
    return x * cos - y * sin, x * sin + y * cos
