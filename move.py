"""bewegungs ableufe"""

import math
import pygame
from config import settings
import vector

CIRCLE = math.pi * 2


class MoveBase:
    """Basis klasse fuer Bewegungen."""

    def __init__(self, pos=(0, 0), radians=0, dater={}, speed=1000):
        """Basis klasse fuer Bewegungen.

        dater=dict mit optionalem Inhalt speed=? sonst 0,
                                         max_angle=? sonst 0.
        """
        self.pos = vector.Rdot(*pos, radians)
        self.speed = dater.get("speed", speed)
        self.max_radian = math.radians(dater.get("max_angle", 0))
        self.animated = False
        self._rotate = True

    def set_pos(self, pos, side="center"):
        """Setzt die position des objects ausgehend von side."""
        setattr(self.rect, side, pos)
        self.pos.pos = self.rect.center

    def rotate(self, radians):
        """rotiert das object um die Mitte."""
        self.pos.rotate(radians)
        if self._rotate and not self.animated:
            degrees = self.pos.degrees
            try:
                self.image = pygame.transform.rotate(self.org_image, -degrees)
            except AttributeError:
                self.org_image = self.image
                self.image = pygame.transform.rotate(self.org_image, -degrees)
            self.rect = self.image.get_rect()
            self.rect.center = self.pos.pos

    def set_rotation(self, radians):
        """Setzt die rotation und rotiert das Bild."""
        self.pos.radians = radians
        if self._rotate and not self.animated:
            degrees = math.degrees(radians)
            try:
                self.image = pygame.transform.rotate(self.org_image, -degrees)
            except AttributeError:
                self.org_image = self.image
                self.image = pygame.transform.rotate(self.org_image, -degrees)
            self.rect = self.image.get_rect()
            self.rect.center = self.pos.pos

    def linear(self, past_time):
        """Gradlinige zeitabhaengige Bewegung"""
        self.pos.move_offset(self.speed * past_time,
                             (-settings["fly_speed"] * past_time, 0))
        self.rect.center = self.pos.pos

    def move(self, radians, past_time):
        """Gradlinige zeitabhaengige Bewegung in Richtung radians."""
        speed_per_frame = self.speed * past_time
        old = self.pos.radians
        self.pos.radians = radians
        self.pos.move(speed_per_frame)
        self.rect.center = self.pos.pos
        self.pos.radians = old

    def move_vector(self, vector):
        """Bewegt das object um den uebergebenen vector"""#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        self.pos.x += vector[0]
        self.pos.y += vector[1]
        self.rect.center = self.pos.pos


def linear(self, past_time):
    """Gradlinige Bewegung, zum composit von object.movement."""
    self.linear(past_time)


def follow(self, past_time):
    """Waehlt die Richtung in der sich das object dreht.

    setzt follow_neg oder follow_pos,
    je nach Position des ziel objects fuer movement.
    """
    try:
        diff = vector.difference(self.pos, self.ziel.pos)
        if diff[1] > 0:
            if diff[0] > 0:
                self.movement = follow_pos
                follow_pos(self, past_time)
            else:
                self.movement = follow_neg
                follow_neg(self, past_time)
        elif diff[1] < 0:
            if diff[0] > 0:
                self.movement = follow_neg
                follow_neg(self, past_time)
            else:
                self.movement = follow_pos
                follow_pos(self, past_time)
        else:
            linear(self, past_time)
    except AttributeError:
        self.movement = linear
        linear(self, past_time)


def follow_total(self, past_time):
    """Folgt dem ziel."""
    if self.ziel.killed:
        self.movement = linear
        self.movement(self, past_time)
        return
    max_radian = self.max_radian * past_time
    radians = vector.radian(self.pos, self.ziel.pos)
    diff = (radians - self.pos.radians) % CIRCLE
    if max_radian < diff:
        self.rotate(max_radian if math.pi > diff else -max_radian)
    elif diff != 0:
        self.set_rotation(radians % CIRCLE)
    self.linear(past_time)


def follow_pos(self, past_time):
    """Folgt dem ziel in positiver Richtung."""
    if self.ziel.killed:
        self.movement = linear
        self.movement(self, past_time)
        return
    max_radian = self.max_radian * past_time
    radians = vector.radian(self.pos, self.ziel.pos)
    diff = (radians - self.pos.radians) % CIRCLE
    if max_radian < diff:
        self.rotate(max_radian)
    elif diff != 0:
        self.movement = follow_total
    self.linear(past_time)


def follow_neg(self, past_time):
    """Folgt dem ziel in negativer Richtung."""
    if self.ziel.killed:
        self.movement = linear
        self.movement(self, past_time)
        return
    max_radian = self.max_radian * past_time
    radians = vector.radian(self.pos, self.ziel.pos)
    diff = (radians - self.pos.radians) % CIRCLE
    if max_radian < diff:
        self.rotate(-max_radian)
    elif diff != 0:
        self.movement = follow_total
    self.linear(past_time)


def random_move(self, past_time):
    # random.choice([up_down, linear]])
    self.movement = linear
    linear(self, past_time)


def up_down(self, past_time):
    pass


moves = {"up_down": up_down,
         "follow": follow,
         "linear": linear,
         "random": random_move}
