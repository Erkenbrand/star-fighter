
import random
import os
import math

import pygame

from tools import open_yaml
import move
import image
import animator
import effects
import pyplay


class Base(pygame.sprite.Sprite, move.MoveBase):
    """Basis von dynamische Objekten."""

    def __init__(self, game, gruppen, dater):
        """Basis von dynamische Objekten."""
        pygame.sprite.Sprite.__init__(self,  *gruppen),
        move.MoveBase.__init__(self, (0, 0), 0, dater)
        self.game = game
        self.movement = self.linear
        self.sound_explusion = os.path.join("daten", "Sound",
                                            dater["sound_explusion"])
        self.explosion_image = image.get(
            os.path.join("daten", "effects", "Images",
                         dater["explosion_image"]))
        self.explosion = dater["explosion"]
        self.killed = False

    @property
    def energie(self):
        return self._energie

    def update(self, past_time):
        if not self.killed:
            if self.game.live_field.contains(self.rect):
                self.movement(past_time)
            else:
                self.kill()
        else:
            effects.Effect((self.game.alle,), self.rect.center,
                           self.explosion_image, 100)
            if self.explosion:
                for tile in self.explosion:
                    # FIXME: Datei nicht jedesmahl oeffnen
                    teil = open_yaml(os.path.join("daten", "truemmer", tile))
                    radians = math.radians(random.randrange(0, 360))
                    teil["speed"] += random.randrange(200)/1000
                    drum = Truemmer(self.game, (self.game.alle,
                                                self.game.truemmer), teil)
                    drum.set_pos(self.rect.center, "center")
                    drum.set_rotation(radians)
            pyplay.play(self.sound_explusion)
            self.kill()


class Truemmer(Base):
    """Teile die durch die Weld fliegen und nicht beruert werden duerfen."""

    def __init__(self, game, gruppen, dater):
        """Teile die durch die Weld fliegen und nicht beruert werden duerfen."""
        Base.__init__(self, game, gruppen, dater)
        self._energie = dater["energie"]
        if dater.get("animation", False):
            self.image = image.get(os.path.join("daten", "truemmer", "Sheets",
                                                dater["image"]))
            animator.linear(self, self.image, dater["animation"])
        else:
            self.image = image.get(os.path.join("daten", "truemmer", "Images",
                                                dater["image"]))
        self.rect = self.image.get_rect()

    @Base.energie.setter
    def energie(self,  value):
        self._energie = value
        if self._energie < 1:
            self.killed = True


class Wall(Base):
    """Mauer die zerstoerbar oder unzerstoerbar sein kann."""

    def __init__(self, game, gruppen, dater):
        """Mauer die zerstoerbar oder unzerstoerbar sein kann."""
        Base.__init__(self, game, gruppen, dater)
        if dater["energie"]:
            self._energie = dater["energie"]
            self.destroiable = True
        else:
            self._energie = 0
            self.destroiable = False
        self.image = image.get(os.path.join("daten", "wall", "Images",
                                            dater["image"]))
        self.rect = self.image.get_rect()
        self._rotate = False

    @Base.energie.setter
    def energie(self,  value):
        if self._energie == 0:
            return
        self._energie = value
        if self._energie < 1:
            self.killed = True
