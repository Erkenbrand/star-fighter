"""Unendliches Zufallslevel."""

import os
from random import randint

import starfield
from tools import open_yaml


class Random_timer():
    time = 0

    def __init__(self, event_type, dater, time_min, time_max,):
        """Zeitverwaltung zum erstellen eines spiel objects."""

        self.event_type = event_type
        self.dater = dater
        self.time_min = time_min
        self.time_max = time_max
        self.time = randint(self.time_min, self.time_max)

    def update(self, past_time):
        """Erstellt nach Ablauf der zeit ein object."""

        if self.time < Random_timer.time:
            self.time += randint(self.time_min, self.time_max)
            self.event_type(self.dater)


class Prototyp_level:

    def __init__(self, game, resolution):
        """Ein Prototyp eines Levels zum testen des Spiels."""

        # das Level des Spiels
        Random_timer.time = 0
        level_dater = open_yaml(os.path.join("daten", "Level", "Random.yml"))
        self.timeline = set()
        for item in level_dater["items"]:
            name, time_min, time_max = item
            dater = open_yaml(os.path.join("daten", "items", name))
            self.timeline.add(Random_timer(game.add_item, dater, time_min,
                                           time_max))
        for wall in level_dater["Wall"]:
            name, time_min, time_max = wall
            dater = open_yaml(os.path.join("daten", "wall", name))
            self.timeline.add(Random_timer(game.add_wall, dater, time_min,
                                           time_max))
        for drum in level_dater["Truemmer"]:
            name, time_min, time_max = drum
            dater = open_yaml(os.path.join("daten", "truemmer", name))
            self.timeline.add(Random_timer(game.add_drum, dater, time_min,
                                           time_max))
        for enemy in level_dater["Feinde"]:
            name, time_min, time_max = enemy
            dater = open_yaml(os.path.join("daten", "Feinde", name))
            for geschuetz_dater in dater["geschütze"]:
                geschuetz_dater["gun"][0] = open_yaml(os.path.join(
                    "daten", "Waffen", geschuetz_dater["gun"][0]))
            self.timeline.add(Random_timer(game.add_enemy, dater, time_min,
                                           time_max))
        # der hintergrund des Spiels
        hintergrund = open_yaml(
            os.path.join("daten", "Hintergruende",
                         level_dater["Hintergrund"] + ".yml"))
        game.set_backgrund(starfield.Star_field(resolution, fps=game.fps,
                                                **hintergrund["Starfield"]))
        game.add_backgrund(open_yaml(os.path.join("daten", "Hintergruende",
                                                  "planet.yml")), (1200, 300))

    def update(self, past_time):
        Random_timer.time += past_time
        for timer in self.timeline:
            timer.update(past_time)
