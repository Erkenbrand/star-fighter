import math
from ship import Ship

class Player:
    """Repraesentiert einen Spieler."""

    def __init__(self):
        """Repraesentiert einen Spieler."""
        self.geld = 0
        self.ship = None
        self.ships = []
        self._select = None
        self.weapons = []
        self.game = None

    def have_ship(self, ship):
        """Testet ob ein Schiff mit dem Namen name,

        im Inventar des Spielers ist.
        """
        for ship_have in self.ships:
            if ship["name"] == ship_have["name"]:
                return True
        return False

    def add_ship(self, ship):
        """Fuegt ein neues Schiff dem Inventar hinzu.

        Setzt ein Schiff als ausgewaehlt wen noch keins gewaehlt ist.
        """
        if not self._select:
            self._select = ship
        if not self.have_ship(ship):
            self.ships.append(ship)

    def select_ship(self, ship):
        """Setzt das ship als selektiert."""
        self._select = ship

    def set_game(self, game):
        """Setzt das naechste spiel."""
        self.game = game
        self.ship = Ship(self._select)
        self.ship.game = game

    def translate(self, cost):
        """Testet ob der Spieler genug geld hat.

        Zieht dann den uebergebenen preis cost vom Besitz des Spielers ab.
        Gibt bei Erfolg True und bei Misserfolg False zurueck.
        """
        if self.geld >= cost:
            self.geld -= cost
            return True
        return False

    @property
    def energie(self):
        return self.ship.energie

    def update(self):
        pass

    def move_up(self, past_time):
        self.ship.move(math.radians(-90), past_time)
        if self.ship.rect.top < 0:
            self.ship.move_vector((0, -self.ship.rect.top))

    def move_down(self, past_time):
        self.ship.move(math.radians(90), past_time)
        if self.ship.rect.bottom > self.game.screen_array.h:
            self.ship.move_vector(
                (0, self.game.screen_array.h - self.ship.rect.bottom))

    def move_left(self, past_time):
        self.ship.move(math.radians(180), past_time)
        if self.ship.rect.left < 0:
            self.ship.move_vector((-self.ship.rect.left, 0))

    def move_right(self, past_time):
        self.ship.move(0, past_time)
        if self.ship.rect.right > self.game.screen_array.w:
            self.ship.move_vector(
                (self.game.screen_array.w - self.ship.rect.right, 0))
        self.ship.fly((self.game.particle, self.game.alle))

    def next_weapon(self):
        """Wechselt im Schiff zur naechsten Waffe."""
        self.ship.next_weapon()

    def previous_weapon(self):
        """Wechselt im Schiff zur vorherigen Waffe."""
        self.ship.previous_weapon()

    def feuer(self, mouse_pos):
        """Feuert alle Geschuetztuerme ab die abgekuelt sind."""
        for feint in self.game.feinde:
            if feint.rect.collidepoint(mouse_pos):
                ziel = feint
                break
        else:
            for teil in self.game.truemmer:
                if teil.rect.collidepoint(mouse_pos):
                    ziel = teil
                    break
            else:
                ziel = None
        self.ship.feuer((self.game.geschosse, self.game.alle), mouse_pos, ziel)
