
import pygame

import animator


class Effect(pygame.sprite.Sprite):
    """Einfacher unbeweglicher Effect der animiert sein kann."""

    def __init__(self, gruppen, pos, image, time=100, animation=False):
        """Einfacher unbeweglicher Effect der animiert sein kann."""
        pygame.sprite.Sprite.__init__(self, *gruppen)
        self.image = image
        if animation:
            animator.linear(self, image, animation)
        self.rect = self.image.get_rect(center=pos)
        self.lifetime = time
        self.rotate = False

    def update(self, past_time):
        if self.lifetime > 0:
            self.lifetime -= past_time
        else:
            self.kill()


class Effectlink(pygame.sprite.Sprite):
    """Einfacher Effect der dem uebergebenen rect folgt."""

    def __init__(self, gruppen, rect, image, time=100):
        """Einfacher Effect der dem uebergebenen rect folgt."""
        pygame.sprite.Sprite.__init__(self, gruppen)

        self.image = image
        self.rect = rect
        self.lifetime = time

    def update(self, past_time):
        if self.lifetime > 0:
            self.lifetime -= past_time
        else:
            self.kill()
