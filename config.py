"""Einstellungen des Spieles."""

import os

from pygame.locals import FULLSCREEN
from pygame.freetype import SysFont

import pyogi
import tools


DEFAULTFONT = SysFont(None, 16)

settings = tools.open_yaml(os.path.join("config.yml"))


class Configdialog(pyogi.Frame):
    """Dialog zum einstellen des Spieles."""

    def __init__(self, updater, size=(200, 200), **config):
        """Dialog zum einstellen des Spieles."""
        self.container = pyogi.Container(size, **config)
        self.updater = updater
        pyogi.Frame.__init__(self, self.container, width=3, **config)
        self.audio_text = pyogi.Text("Musik : ", font=DEFAULTFONT,
                                     text_fg=(255, 255, 255))

        if settings["flags"] & FULLSCREEN:
            self.fullscreen = pyogi.Checkbutton(
                label=pyogi.Text("Fullscreen", font=DEFAULTFONT),
                event=self.fullscreen_swish)
            self.fullscreen.state = True
        else:
            self.fullscreen = pyogi.Checkbutton(
                label=pyogi.Text("Windowed", font=DEFAULTFONT),
                event=self.fullscreen_swish)
            self.fullscreen.state = False
        if settings["music"]:
            self.audio = pyogi.Checkbutton(size=(25, 25),
                                           label=pyogi.Text(
                                               "<))", font=DEFAULTFONT),
                                           event=self.music_swish)
            self.audio.state = False
        else:
            self.audio = pyogi.Checkbutton(size=(25, 25),
                                           label=pyogi.Text("<-",
                                                            font=DEFAULTFONT),
                                           event=self.music_swish)
            self.audio.state = True
        self.config_text = pyogi.Text("FPS : ", font=DEFAULTFONT,
                                      text_fg=(255, 255, 255))
        self.config_fps = pyogi.XBar(maximal=120, minimal=30, step=5,
                                     font=DEFAULTFONT,
                                     text_fg=(255, 255, 255),
                                     widget_fg=(255, 100, 100, 160),
                                     widget_bg=(100, 100, 20, 220))
        self.config_fps.set(settings["fps"])
        self.save_button = pyogi.Button(label=pyogi.Text(
            "übernehme", font=DEFAULTFONT), event=self.save)
        self.exit_button = pyogi.Button(label=pyogi.Text("exit", font=DEFAULTFONT),
                                        event=self.exit)

        self.container.place(self.audio_text, (0, 5), "topleft")
        self.container.place(self.save_button, (0, 0), "bottomright")
        self.container.place(self.exit_button, (0, 0), "bottomleft")
        pyogi.place.to(self.audio_text, self.audio, "midleft", "midright")
        pyogi.place.to(self.audio_text, self.config_text, "topleft",
                       "bottomleft", (0, 20))
        pyogi.place.to(self.config_text, self.config_fps,
                       "midleft", "midright")
        pyogi.place.to(self.config_text, self.fullscreen, "topleft",
                       "bottomleft", (0, 20))

    def exit(self):
        """Beendet den Dialog ohne zu speichern."""
        self.toggle_visibility()
        if settings["music"]:
            self.audio._slave.text = "<))"
            self.audio.state = False
        else:
            self.audio._slave.text = "<-"
            self.audio.state = True
        self.config_fps.set(settings["fps"])

    def fullscreen_swish(self, value):
        """Wechselt zwischen Fullscreen und Windowed."""
        if value:
            self.fullscreen._slave.text = "Fullscreen"
        else:
            self.fullscreen._slave.text = "Windowed"

    def music_swish(self, value):
        """Setzt den text der Musikschaltfläche."""
        if value:
            self.audio._slave.text = "<-"
        else:
            self.audio._slave.text = "<))"

    def save(self):
        """Beendet den Dialog und speichert die Einstellungen.

        Ruft die updater Funktion des Spieles auf.
        Speichert die einstellungen in config.yml.
        """
        self.toggle_visibility()
        if self.audio.state:
            settings["music"] = False
        else:
            settings["music"] = True
        settings["fps"] = self.config_fps.get()
        if self.fullscreen.state:
            settings["flags"] = FULLSCREEN
        else:
            settings["flags"] = 0
        tools.dump_yaml(settings, os.path.join("config.yml"))
        self.updater()
