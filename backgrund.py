import os

import pygame

import move
import image


class Backgrund(pygame.sprite.Sprite, move.MoveBase):
    """Object im Hintergrund des Spieles."""

    def __init__(self, game, gruppen, dater):
        """Object im Hintergrund des Spieles."""
        pygame.sprite.Sprite.__init__(self, *gruppen)
        move.MoveBase.__init__(self, (0, 0), 0, dater)
        self.game = game
        self.movement = self.linear
        if dater.get("animation", False):
            self.image = image.get(os.path.join("daten", "Hintergruende",
                                                "Sheets", dater["image"]))
            animator.linear(self, self.image, dater["animation"])
        else:
            self.image = image.get(os.path.join("daten", "Hintergruende",
                                                "Images", dater["image"]))
        self.rect = self.image.get_rect()
        self.rotate = False

    def update(self, past_time):
        if self.rect.right >= 0:
            self.movement(past_time)
        else:
            self.kill()
