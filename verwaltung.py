"""Haendler und Inventar Verwaltung."""

import os

import pygame

import pyogi
from pyogi.theme import dark
from pyogi.locals import *

import pyworld

from config import DEFAULTFONT, settings
from random_level import Prototyp_level
import image
import game
import menu

import inventory
import trader


class Administration(pyworld.Scene, inventory.Inventory, trader.Trader):
    """Haendler und Inventar Verwaltung."""

    def __init__(self, player, angebot={"weapons": (), "ships": ()}):
        """Haendler und Inventar Verwaltung."""
        pyworld.Scene.__init__(self)
        inventory.Inventory.__init__(self)

        self.settings = settings
        self.angebot = angebot
        self.player = player
        self.holo = None
        self.selectet = None
        self.weapon_mark = None
        self.right_bar = pyogi.Container(size="auto", sorter=pyogi.sort.Row())
        self.left_bar = pyogi.Container(size="auto", sorter=pyogi.sort.Row())
        self.background = pygame.transform.scale(pygame.image.load(
            os.path.join("daten", "Hintergruende", "Images",
                         "shipconf.png")), settings["resolution"]).convert()

        self.gui = pyogi.Gui(settings["resolution"], theme=dark)
        self.admin = pyogi.basics.Admin(self.gui)
        self.back = pyogi.Button(pyogi.Text("Zurück", DEFAULTFONT),
                                 event=self.menu)
        self.start = pyogi.Button(pyogi.Text("Start", DEFAULTFONT),
                                  event=self.start_game)
        self.inventory = pyogi.Checkbutton(pyogi.Text("inventory", DEFAULTFONT),
                                           event=self.show_inventory)
        self.trader = pyogi.Checkbutton(pyogi.Text("händler", DEFAULTFONT),
                                        event=self.show_trader)
        self.inventory.connect(self.trader)
        self.geld = pyogi.Text(f"${self.player.geld}", DEFAULTFONT,
                               text_fg=(255, 255, 0))

        self.gui.add_all((self.inventory, self.trader, self.back, self.start,
                          self.left_bar, self.right_bar))
        self.inventory.link_size(self.gui, self.gui, "midtop", "topleft",
                                 mode="w")  #TODO: reihnefolge änderbar machen(gui)
        self.trader.link_size(self.gui, self.gui, "midtop", "topright",
                              mode="w")
        self.back.link_size(self.gui, self.gui, "midbottom", "bottomleft",
                            mode="w")
        self.start.link_size(self.gui, self.gui, "midbottom", "bottomright",
                             mode="w")

        self.left_bar.link_size(self.back, self.inventory, "topleft",
                                "bottomleft", mode="h")
        self.right_bar.link_size(self.back, self.trader, "topright",
                                 "bottomright", mode="h")
        pyogi.place.to(self.inventory, self.geld, "midtop", "bottomright")


    def next_layer(self):
        """Wechselt auf eine neue Waffenebene."""
        if self.selectet:
            self.weapon_layer += 1
            self.weapon_layer_label.text = str(self.weapon_layer)
            self.set_guns(self.selectet[1])

    def previous_layer(self):
        """Wechselt auf eine vorherige Waffenebene."""
        if self.selectet:
            self.weapon_layer -= 1
            if self.weapon_layer < 1:
                self.weapon_layer = 1
            else:
                self.weapon_layer_label.text = str(self.weapon_layer)
                self.set_guns(self.selectet[1])

    def clean_gui(self):
        """reinigt die gui um zwischen Inventar und Haendler umzuschalten."""
        self.right_bar.clean()
        self.left_bar.clean()
        self.selectet = None
        self.admin.clean()
        pyogi.dialog.clean()

    def start_game(self):
        """Startet das spiel und rueumt die Gui auf."""
        if not self.player._select:
            pyogi.popup.set(pyogi.Text("!!Kein Schiff gewählt!!", DEFAULTFONT,
                                       text_bg=(200, 200, 100, 80),
                                       text_fg=(100, 20, 20)),
                            self.gui.get_side("center"), "center")
            pyogi.timer.clean()
            pyogi.timer.add(pyogi.popup.clean,  5000, once=True)
            return
        pyogi.dialog.clean()
        pyogi.popup.clean()
        pyogi.timer.clean()
        image.clean()
        self.new_scene(game.Game(Prototyp_level, self.player))

    def menu(self):
        """Geht zurueck zum Hauptmenue und rueumt die Gui auf."""
        pyogi.dialog.clean()
        pyogi.popup.clean()
        pyogi.timer.clean()
        image.clean()
        self.new_scene(menu.Menu())

    def update(self, past_time):
        self.gui.update()

    def draw(self, image):
        if self.gui.changed:
            image.blit(self.background, (0, 0))
            self.gui.draw(image)
            if self.weapon_mark:
                self.weapon_mark(image, self.holo.get_side("center"))

    def event(self, event):
        self.gui.test_event(event)
