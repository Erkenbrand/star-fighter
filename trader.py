import os

from tools import open_yaml

import pyogi
from pyogi.locals import *

from config import DEFAULTFONT
from inventory import compare_price



class Trader:
    """Initialisiert den Händler."""

    def show_trader(self, state):
        """Initialisiert den Händler."""

        if not state:
            return
        self.clean_gui()
        for layer in (self.weapon_next, self.weapon_layer_label,
                      self.weapon_previous):
            layer.kill()
        # add ships too buy
        path = os.path.join("daten", "Schiffe")
        simbols = []
        ships = [open_yaml(os.path.join(path, ship)
                           ) for ship in self.angebot["ships"]]
        for ship in sorted(ships, key=compare_price):
            if self.player.have_ship(ship):
                continue
            simbol = pyogi.images.Image(os.path.join(path, "Images",
                                                     ship["image"]))
            simbol.bind_event_mode(MOUSEBUTTONUP, 1, self.select_ship(simbol,
                                                                      ship,
                                                                      True))
            simbols.append(simbol)
        self.left_bar.add_all(simbols)

        # add weapons too buy
        path = os.path.join("daten", "Waffen")
        simbols = []
        weapons = [open_yaml(os.path.join(path, weapon)
                             ) for weapon in self.angebot["weapons"]]
        for weapon in sorted(weapons, key=compare_price):
            simbol = pyogi.images.Image(os.path.join(path, "Images",
                                                     weapon["image"]))
            frame = pyogi.Frame(simbol, self.gun_icon.get_size())
            frame.bind_event_mode(MOUSEBUTTONUP, 1, self.select_weapon(frame,
                                                                       weapon))
            simbol.bind_event_mode(MOUSEBUTTONUP, 1, self.select_weapon(frame,
                                                                        weapon)
                                   )
            simbols.append(frame)
        self.right_bar.add_all(simbols)

    def select_weapon(self, simbol, dater):
        """Zeigt bei Auswahl einer Waffe den Kaufdialog an."""

        def select():
            if self.selectet:
                if self.selectet[1] == dater:
                    return
                self.selectet[0].remove_composer()
            self.admin.clean()
            simbol.composit(pyogi.composit.Frame(collor=(255, 0, 0)))
            self.selectet = (simbol, dater)
            self.buy_weapon_dialog(dater)
        return select

    def buy_weapon_dialog(self, dater):
        """Ein Dialog zum kaufen der Waffe.

        Zeigt alle Daten zur Waffe an.
        """

        buy = pyogi.Button(pyogi.Text("buy", DEFAULTFONT),
                           event=self.buy_weapon)
        number = 0
        for weapon in self.player.weapons:
            if weapon["name"] == dater["name"]:
                number += 1
        self.number = pyogi.Text(f"x {number}", DEFAULTFONT,
                                 text_bg=(0, 0, 0, 125))
        info = pyogi.Textbox(
            f"Name : {dater['name']}\nPreis : ${dater['price']}\
\nSchaden : {dater['schaden']}\nNachladezeit : {dater['hitze']}ms\
\nGeschwindigkeit : {dater['speed']}\nStufe : {dater['stufe']}",
            DEFAULTFONT, offset=(0, 10), widget_bg=(0, 0, 0, 125))
        self.admin.add(buy)
        self.admin.add(info)
        self.admin.add(self.number)
        buy.link_side(self.right_bar, "topright", "topleft")
        info.link_side(buy, "topright", "bottomright")
        self.number.link_side(info, "bottomleft", "topleft")

    def buy_weapon(self):
        """Kauft die Waffe und fueght sie dem Spieler hinzu."""

        dater = self.selectet[1]
        if self.player.translate(dater['price']):
            self.geld.text = f"${self.player.geld}"
            self.player.weapons.append(dater)
            number = 0
            for weapon in self.player.weapons:
                if weapon["name"] == dater["name"]:
                    number += 1
            self.number.text = f"x {number}"

    def buy_ship_dialog(self, dater):
        """Ein Dialog zum kaufen eines Schiffes.

        Zeigt alle Daten zum Schiff an.
        """

        buy = pyogi.Button(pyogi.Text("buy", DEFAULTFONT), event=self.buy_ship)
        text = f"Name : {dater['name']}\n\
Preis : ${dater['price']}\nRüstung : {dater['energie']}\n\
Schild : {dater['schild']}\n\
Schildladeverzögerung : {dater['schild_colddown']}\n[Geschütze]\n"
        raketentuerme = 0
        energietuerme = 0
        gemischtetuerme = 0
        for geschuetz in dater['geschütze']:
            if geschuetz["kind"] == "r":
                raketentuerme += 1
            elif geschuetz["kind"] == "e":
                energietuerme += 1
            else:
                gemischtetuerme += 1
        if energietuerme:
            text += f"Energietürme : {energietuerme}\n"
        if raketentuerme:
            text += f"Raketentürme : {raketentuerme}\n"
        if gemischtetuerme:
            text += f"Energie/Raketentürme : {gemischtetuerme}\n"
        info = pyogi.Textbox(text, DEFAULTFONT, offset=(0, 10),
                             widget_bg=(0, 0, 0, 125))
        self.admin.add(buy)
        self.admin.add(info)
        buy.link_side(self.left_bar, "topleft", "topright")
        info.link_side(buy, "topleft", "bottomleft")

    def buy_ship(self):
        """Kauft das SCHiff und fueght es dem Spieler hinzu."""

        dater = self.selectet[1]
        if self.player.translate(dater['price']):
            self.geld.text = f"${self.player.geld}"
            self.player.add_ship(dater)
            self.selectet[0].kill()
            self.selectet = None
            self.admin.clean()

    def set_holo(self):
        """Setzt ein hologram in die Mitte des Fensters."""
        size = self.selectet[0].size
        holo = pyogi.images.Animation(
            os.path.join("daten", "Schiffe", "Images",
                         self.selectet[1]["holo"]),
            (size[0]*2, size[1]*2), 2, 5)
        self.holo = holo
        self.admin.place(holo, (0, 0), "center", "center")
