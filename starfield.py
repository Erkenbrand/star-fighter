from random import randint

import pygame

def new(star, array, x_size, y_size):
    x, y, color, speed = star
    array[int(x)][y] = color
    x_p = x - speed
    if x_p < 0:
        return (x_size, randint(0, y_size), color, speed)
    return (x_p, y, color, speed)


class Star_field:

    def __init__(self, size=(200, 200), dichte=200, ebenen=3, fps=60,
                 speed=50):
        self.stars = set()
        self.size = (size[0] - 1, size[1] - 1)
        for ebene in range(ebenen):
            for d in range(dichte // (ebene + 1)):
                self.stars.add((randint(0, int(size[0]) - 1),
                                randint(0, size[1] - 1),
                                int(0xffffff - 0x3f3f3f * (ebenen - ebene)),
                                (ebene + 1) / fps * speed))

    def draw(self, buffer):
        array = pygame.PixelArray(buffer)
        self.stars = {new(i, array, *self.size) for i in self.stars}
