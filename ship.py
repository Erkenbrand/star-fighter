import os

import pygame

import image
import move
from effects import Effect, Effectlink
from geschuetze import Geschuetz


class Ship(pygame.sprite.Sprite, move.MoveBase):
    """Ein Schiff das fliegen kann.

    Stoest flamen an Triebwerken aus und hat Geschuetze zum feuern.
    """

    def __init__(self, dater):
        """Ein Schiff das fliegen kann.

        Stoest flamen an Triebwerken aus und hat Geschuetze zum feuern.
        """
        pygame.sprite.Sprite.__init__(self)
        self.name = dater["name"]
        self._energie = dater["energie"]
        self.max_energie = self._energie
        self._schild = dater["schild"]
        self.max_schild = self._schild
        self.schild_colddown = dater["schild_colddown"]
        self.schild_time = 0
        self.geschuetze = []
        self.game = None
        self.holo = dater["holo"]
        for geschuetz in dater["geschütze"]:
            self.geschuetze.append(Geschuetz(geschuetz))
        self.explosion_image = image.get(
            os.path.join("daten", "effects", "Images",
                         dater["explosion_image"])
            )
        self.fire = set()
        for duese in dater["düse"]:
            self.fire.add((image.get(os.path.join("daten", "effects", "Sheets",
                                                  duese["image"])),
                           tuple(duese["pos"])))
        self.image = image.get(os.path.join("daten", "Schiffe", "Images",
                                            dater["image"]))
        self.image_schild = image.get(
            os.path.join("daten", "Schiffe", "Images", dater["image_schild"]))
        self.rect = self.image.get_rect()
        move.MoveBase.__init__(self, pos=self.rect.center, dater=dater)

    @property
    def energie(self):
        return self._energie

    @energie.setter
    def energie(self, value):
        self._energie = value
        if self._energie > self.max_energie:
            self._energie = self.max_energie

    @property
    def schild(self):
        return self._schild

    def treffer(self, value, grupe):
        """Berechnet den schaden des Schiffes bei einem Treffer."""
        new = self._schild + value
        if new > self.max_schild:
            new = self.max_schild
        elif new < 0:
            if self._schild:
                Effectlink(grupe, self.rect, self.image_schild, 500)
            self.energie += value
            new = 0
        elif new < self._schild:
            Effectlink(grupe, self.rect, self.image_schild, 500)
        self._schild = new

    def update(self, past_time):
        for geschuetz in self.geschuetze:
            geschuetz.update(past_time)
        self.schild_time -= past_time
        if self.schild_time < 0:
            self.schild_time = self.schild_colddown
            self.treffer(1, None)

    def fly(self, group):
        """Stoeßt Partikel an den Duesen aus."""
        for fire in self.fire:
            pos = (self.rect.x + fire[1][0], self.rect.y + fire[1][1])
            Effect((group,), pos, fire[0], 50,
                   {"w": 12, "h": 12, "delay": 9, "items": 5})

    def next_weapon(self):
        """Wechselt zur naechsten Waffe."""
        for geschuetz in self.geschuetze:
            geschuetz.next()

    def previous_weapon(self):
        """Wechselt zur vorherigen Waffe."""
        for geschuetz in self.geschuetze:
            geschuetz.previous()

    def feuer(self, group, ziel_pos, ziel):
        """Feuert alle Geschuetze ab."""
        for geschuetz in self.geschuetze:
            geschuetz.feuer(group, self.pos, ziel_pos, ziel, self.game)
