import os
import sys

import pygame

import pyogi
from pyogi.theme import dark

import pyworld

from verwaltung import Administration
from player import Player
from config import DEFAULTFONT, settings, Configdialog


class Menu(pyworld.Scene):
    """Das Hauptmenue des Spiels."""

    def __init__(self):
        """Das Hauptmenue des Spiels."""
        pyworld.Scene.__init__(self, settings["menu_fps"])
        resolution = settings["resolution"]
        pygame.mouse.set_cursor(*pygame.cursors.arrow)
        self.gui = pyogi.Gui(resolution, theme=dark)
        self.random_level = pyogi.Button(label=pyogi.Text("Zufallslevel",
                                                          font=DEFAULTFONT),
                                         event=self.start_random)
        self.exit = pyogi.Button(label=pyogi.Text("EXIT", font=DEFAULTFONT),
                                 event=sys.exit)
        self.configdialog = Configdialog(self.update_settings,
                                         (resolution[0] / 2,
                                          resolution[1] / 2))
        self.configdialog.toggle_visibility()
        self.config = pyogi.Button(
            label=pyogi.Text("Config", font=DEFAULTFONT),
            event=self.configdialog.toggle_visibility)
        self.gui.place(self.random_level, (0, -100), "center", "center")
        pyogi.place.to(self.random_level, self.config, "midtop", "midbottom",
                       offset=(0, 10))
        pyogi.place.to(self.config, self.exit, "midtop", "midbottom",
                       offset=(0, 10))
        self.background = pygame.transform.scale(
            pygame.image.load(os.path.join("daten", "Hintergruende", "Images",
                                           "Hintergrund.png")),
            resolution).convert()
        self.gui.place(self.configdialog, (0, 0), "midtop", "midtop")
        self.update_settings()

    def update_settings(self):
        """Aktualisiert das Programm nach den Einstellungen in Configdialog."""
        #  TODO: schreiben eines Themen basierten Musikspieler.
        if settings["music"] and not pygame.mixer.music.get_busy():
            pygame.mixer.music.load(
                os.path.join("daten", "Musik", "menue_final.ogg"))
            pygame.mixer.music.play(-1)
        elif not settings["music"]:
            pygame.mixer.music.stop()
        self.set_flags(settings["flags"])

    def start_random(self):
        """Startet ein unendliches Zufalls generiertes level."""
        weapons = set()
        for file in os.listdir(os.path.join("daten", "Waffen")):
            if os.path.splitext(file)[1] == ".yml":
                weapons.add(file)
        ships = set()
        for file in os.listdir(os.path.join("daten", "Schiffe")):
            if os.path.splitext(file)[1] == ".yml":
                ships.add(file)
        player = Player()
        player.geld = 1000
        self.new_scene(Administration(player, {"weapons": weapons,
                                               "ships": ships}))

    def update(self, past_time):
        self.gui.update()

    def draw(self, image):
        """Zeichnet den Hintergrund und die Gui."""
        if self.gui.changed:
            image.blit(self.background, (0, 0))
            self.gui.draw(image)

    def event(self, event):
        self.gui.test_event(event)
