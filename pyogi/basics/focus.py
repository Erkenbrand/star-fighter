from pyogi.locals import LOST_FOCUS, SET_FOCUS, FOCUS


class Focus:
    """
    Verwaltet die Fokussierung der Widget. Das aktuel fokussierte Widget
    ist in Focus.active gespeichert.
    """

    active = None

    def update():
        if Focus.active:
            Focus.active.play_event(FOCUS, None)

    def defocussing(self):
        """defokussiert das Widget."""

        if Focus.active == self:
            Focus.active.play_event(LOST_FOCUS, None)
            Focus.active = None

    def focussing(self):
        """Fokussiert das Widget, defokussiert das aktuelle fokussierte Widget.
        """

        if Focus.active != self:
            try:
                Focus.active.play_event(LOST_FOCUS, None)
            except AttributeError as err:
                print("Es wurde kein Widget zum defokussieren gefunden", err)
            Focus.active = self
            self.play_event(SET_FOCUS, None)
