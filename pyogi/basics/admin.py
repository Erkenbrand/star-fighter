class Admin:

    def __init__(self, master):
        self.master = master
        self.widgets = set()

    def add(self, widget):
        self.widgets.add(widget)
        self.master.add(widget)

    def place(self, widget, pos=(0, 0), side="topleft",
              anchor=None, link=False):
        self.widgets.add(widget)
        self.master.place(widget, pos, side, anchor, link)

    def clean(self):
        for widget in self.widgets:
            widget.kill()
        self.widgets.clear()
