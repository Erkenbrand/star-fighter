from inspect import getargspec

from pyogi.locals import *


class MouseIn(Exception):
    """Gibt das angeklickte Widget durch zur Bearbeitung."""

    old_in = set()
    new_in = set()
    widget = None

    def add(widget):
        MouseIn.widget = widget
        MouseIn.new_in.add(widget)

    def __str__(self):
        return "Event fuer " + str(MouseIn.widget) + " active"


class Event(object):
    """Basis event object von dem alle widget erben."""

    def __init__(self):
        self.events = {}
        self._active = True
        self.masterevent = None

    def activate(self):
        self._active = True

    def deactivate(self):
        self._active = False

    def bind_masterevent(self, name, event):
        """Bindet ein event an das widget und den slaves des widgets."""
        self.masterevent = (name, event)
        self.events[name] = event

    def bind_masterevent_mode(self, etype, mode, event):
        """Bindet ein event an das widget und den slaves des widgets."""
        self.bind_masterevent(EVENTSTRING % (etype, mode), event)

    def bind_event_mode(self, etype, mode, event):
        """Bindet ein Event an das widgetself.

        etype : ein pygame Event type (MOUSEMOTION, MOUSEBUTTONUP,
        MOUSEBUTTONDOWN, KEYUP, KEYDOWN)
        mode : die art der eingabe
        bei MOUSEBUTTONUP/DOWN button
        bei MOUSEMOTION buttons
        bei KEYUP/DOWN key
        event : das bei Aktivierung aufzurufende Event
        """
        self.events[EVENTSTRING % (etype, mode)] = event

    def bind_event(self, name, event):
        """Bindet ein event an : Ereignisname."""
        self.events[name] = event

    def clean_events(self):
        """Loescht alle gebundenen events."""
        self.events = {}

    def play_event(self, name, event=None):
        """Fuert das event mit den event name aus."""
        if self.visibility and self._active:
            funktion = self.events.get(name, None)
            if funktion:
                try:
                    if "event" in getargspec(funktion).args:
                        funktion(event)
                    else:
                        funktion()
                except TypeError as err:
                    if "unsupported callable" == str(err):
                        funktion()
                    else:
                        raise
        return False

    def get_event(self, name):
        return self.events[name]
