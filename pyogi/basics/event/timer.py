import pygame


class Delay:

    def __init__(self, event, delay=1000, killer=False):
        """Ein Event, welches das uebergebene Event, erst ausfuehrt, wen die
        angegebene zeit, seit dem letzten aufruf abgelaufen ist.
        """
        self.delay = delay
        self.next_clall = delay + pygame.time.get_ticks()
        self.event = event
        self.killer = killer

    def reset(self):
        """Setzt das timer Wider an den Anfang."""
        self.next_clall = pygame.time.get_ticks() + self.delay

    def __call__(self, *args, **kwargs):
        past_time = pygame.time.get_ticks()
        if past_time >= self.next_clall:
            self.next_clall = past_time + self.delay
            self.event(*args, **kwargs)
            if self.killer:
                self.killer(self)
            return True
        return False


class InputDelayChek:

    def __init__(self, start_delay=1000):
        """Ein Event, welches das uebergebene Event, erst ausfuehrt, wen die
        angegebene zeit, nach wiederholter Eingabe, abgelaufen ist.
        """
        self.start_delay = start_delay
        self.call_time = 0

    def reset(self):
        """Setzt das timer Wider an den Anfang."""
        self.call_time = 0

    def __call__(self, action, *args, **kwargs):

        past_time = pygame.time.get_ticks()
        if not self.call_time:
            action(*args, **kwargs)
            self.call_time = past_time + self.start_delay
        elif self.call_time <= past_time:
            action(*args, **kwargs)


class InputDelay(InputDelayChek):

    def __init__(self, event, start_delay=1000):
        """Ein Event, welches das uebergebene Event, erst ausfuehrt, wen die
        angegebene zeit, nach wiederholter Eingabe, abgelaufen ist.
        """
        self.event = event
        InputDelayChek.__init__(self, start_delay)

    def __call__(self, *args, **kwargs):
        InputDelayChek.__call__(self, self.event, *args, **kwargs)


class Doubleclick:

    def __init__(self, event, singleevent=None, timer=500):
        """Ein Event, welches das uebergebene Event, erst ausfuehrt, wen es
        innerhalb der angegebenen zeit, zweimahl aufgerufen wurde.
        Kann auch als dekorator benutzt werden.
        """
        self.event = event
        self.singleevent = singleevent
        self.last_call = 0
        self.timer = timer

    def __get__(self, obj, type=None):
        return self.__class__(self.event.__get__(obj, type),
                              self.singleevent.__get__(
                                  obj, type) if self.singleevent else None,
                              self.timer)

    def reset(self):
        """Setzt den timer zuruek."""
        self.last_call = pygame.time.get_ticks() - self.timer

    def __call__(self, *args, **kwargs):

        past_time = pygame.time.get_ticks()
        if past_time < self.last_call + self.timer:
            self.event(*args, **kwargs)
            return True
        elif self.singleevent:
            self.singleevent()
        self.last_call = past_time
        return False
