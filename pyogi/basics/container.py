
import pyogi
from pyogi.basics import Widget
from pyogi.basics.event import MouseIn


class BaseContainer(Widget):

    def __init__(self, sorter=None, **config):
        """Ein Container fuer Widget, er zeichnet die Widget,
        in der reihen folge in der sie hinzugefuegt wurden
        """
        Widget.__init__(self, **config)

        self._slaves = []
        self._xscroll = 0
        self._yscroll = 0
        self.sorter = sorter

    def bind_masterevent(self, name, event):
        """Bindet oder ueberschreibt ein event fuer alle slaves des Containers.
        """
        Widget.bind_masterevent(self, name, event)
        for widget in self._slaves:
            widget.bind_event(name, event)

    def autosize(self):
        if self._slaves:
            self.geometry.size = pyogi.sizes.bound_rect(self._slaves).size
        else:
            self.geometry.size = (0, 0)

    def create(self):
        Widget.create(self)
        for widget in self._slaves:
            widget.create()

    def change(self):
        """Gibt das aktuelle Widget zum update frei."""
        if not self.changed and self.alive:
            self._master.change()
            self.changed = True

    def draw(self, buffer):
        """Zeichnet auf das uebergebene Surface."""
        for slave in self._slaves:
            slave.draw(buffer)

    def add_all(self, widgets):
        for widget in widgets:
            self.add(widget)

    def add(self, widget):
        """Fuegt das uebergebene widget dem Container hinzu."""
        Widget.add(self, widget)
        self._slaves.append(widget)
        self.update_geometry()

    def xscroll(self, value=0):
        self.set_xscroll(self._xscroll + value)

    def yscroll(self, value=0):
        self.set_yscroll(self._yscroll + value)

    def set_yscroll(self, value=0):
        self._yscroll = value
        self.change()

    def reset_scroll(self):
        self._yscroll = 0
        self._xscroll = 0
        self.change()

    def place(self, widget, pos=(0, 0), side="topleft", anchor=None,
              link=False):
        """Platziert das Widget an der uebergebenen Position.

        side : gibt die Seite an die platziert wird
        anchor : gibt die Seite des Containers an,
        von der aus das Widget platziert wird
        """
        self.add(widget)
        if not anchor:
            anchor = side
        if link:
            widget.link_side(self, side, anchor, pos)
        else:
            x, y = self.local_side(anchor)
            widget.set_side((pos[0] + x, pos[1] + y), side)
        self.update_geometry()

    def remove(self, widget):
        """Loescht das uebergebenen Widget aus dem Container."""
        self._slaves.remove(widget)
        widget._master = None
        self.update_geometry()

    def mouse_test(self, pos):
        """Prueft die Position im Container, und in den Widgets."""
        try:
            Widget.mouse_test(self, pos)
        except MouseIn:
            for slave in reversed(self._slaves):
                slave.mouse_test(self.mouse_pos)
            raise

    # def kill(self):
    #     """Loescht alle Widget und den Container."""
    #     for widget in self._slaves[:]:
    #         widget.kill()
    #     Widget.kill(self)

    def clean(self):
        """Loescht alle Widget."""
        for widget in self._slaves[:]:
            widget.kill()
