import pygame

from pyogi.basics import SingleContainer, Widget


class BaseButton(SingleContainer):

    def __init__(self, label, size, orientation, event, state_event, height,
                 **config):
        """Basis eines Buttons, das beim loslassen der linken Maustaste,
        ein Event ausfuehrt.
        """
        self._frame = height
        SingleContainer.__init__(self, size, orientation, **config)
        if label:
            self.add(label)
        self.event = event
        self.state_event = state_event
        self.state = 0
        self.image = None
        self.event_master = True

    def create(self):
        SingleContainer.create(self)
        self._color = self._config.get("widget_bg",
                                       self._theme.colors.widget_bg)

    def update(self):
        SingleContainer.update(self)
        if self.orientation and self._slave:
            frame = self.geometry.inflate(-self._frame * 2 - 1,
                                          -self._frame * 2 - 1)
            frame.topleft = (0, 0)
            side = getattr(frame, self.orientation)
            setattr(self._slave.geometry, self.orientation, side)
        self._image = pygame.Surface(self.geometry.size)
        self._image.fill(self._color)
        if self.state:
            self._theme.button.down(self._image, self._color, self._frame)
            if self._slave:
                frame.topleft = (1 + self._frame, 1 + self._frame)
                SingleContainer.draw(self, self._image.subsurface(frame))
        else:
            self._theme.button.up(self._image, self._color, self._frame)
            if self._slave:
                SingleContainer.draw(self, self._image.subsurface(
                    (self._frame, self._frame,
                     self.w - self._frame * 2, self.h - self._frame * 2)))

    @property
    def fill_size(self):
        """Gibt die Groesse, die von einem widget befuellt werden kann zurueck.
        """

        if self._frame:
            return (self.w - self._frame * 2, self.h - self._frame * 2)
        return self.geometry.size

    def autosize(self):
        if self._slave:
            if self._frame:
                self.geometry.size = (self._slave.w + self._frame * 2 + 1,
                                      self._slave.h + self._frame * 2 + 1)
            else:
                self.geometry.size = self._slave.size
        else:
            if self._frame:
                self.geometry.size = (self._frame * 2 + 1, self._frame * 2 + 1)
            else:
                self.geometry.size = (2, 2)

    def down(self, event=None):
        if not self.state:
            self.state = 1
            self.change()
            if self.state_event:
                self.state_event(self.state)

    def up(self, event=None):
        if self.state:
            self.state = 0
            self.change()
            if self.state_event:
                self.state_event(self.state)

    def release(self, event=None):
        if self.state:
            self.state = 0
            self.change()
            if self.event:
                self.event()
            if self.state_event:
                self.state_event(self.state)

    draw = Widget.draw
