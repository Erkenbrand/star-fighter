import pygame

from pyogi.basics.event import MouseIn
from .widget import Widget


class SingleContainer(Widget):

    def __init__(self, size="fill", orientation="center", **config):
        """Ein Container fuer Widget, er zeichnet die Widget,
        in der reihen folge in der sie hinzugefuegt wurden
        """
        Widget.__init__(self, **config)

        self.geometry = pygame.Rect((0, 0), (0, 0))
        self.orientation = orientation
        self._slave = None
        self.size = size
        self._xscroll = 0
        self._yscroll = 0
        self.event_master = False

    @property
    def slave(self):
        return self._slave

    @slave.setter
    def slave(self, widget):
        self.add(widget)

    def create(self):
        Widget.create(self)

        if self._slave:
            self._slave.create()
        self.update_geometry()

    def autosize(self):
        if self._slave:
            self.geometry.size = self._slave.size
            self.reset_scroll()

    def update(self):
        if self.orientation and self._slave:
            pos = self.local_side(self.orientation)
            setattr(self._slave.geometry, self.orientation,
                    (pos[0] + self._xscroll, pos[1] + self._yscroll))

    def xscroll(self, value=0):
        self.set_xscroll(self._xscroll + value)

    def yscroll(self, value=0):
        self.set_yscroll(self._yscroll + value)

    def set_xscroll(self, value=0):
        slave_geometry = self._slave.geometry
        diver = self._xscroll - value

        if slave_geometry.x < 0 and diver < 0:
            if slave_geometry.x < diver:
                self._xscroll = value
            else:
                self._xscroll -= slave_geometry.x
            self.change()
        elif slave_geometry.right > self.geometry.w and diver > 0:
            right = slave_geometry.right - self.geometry.w
            if right > diver:
                self._xscroll = value
            else:
                self._xscroll -= right
            self.change()

    def set_yscroll(self, value=0):
        slave_geometry = self._slave.geometry
        diver = self._yscroll - value
        if slave_geometry.y < 0 and diver < 0:
            if slave_geometry.y < diver:
                self._yscroll = value
            else:
                self._yscroll -= slave_geometry.y
            self.change()
        elif slave_geometry.bottom > self.geometry.h and diver > 0:
            bottom = slave_geometry.bottom - self.geometry.h
            if bottom > diver:
                self._yscroll = value
            else:
                self._yscroll -= bottom
            self.change()

    def reset_scroll(self):
        self._yscroll = 0
        self._xscroll = 0
        self.change()

    def draw(self, buffer):
        """Zeichnet auf das uebergebene Surface."""

        if self._slave:
            self._slave.draw(buffer)

    def add(self, widget):
        """Fuegt das uebergebene widget dem Container hinzu."""

        Widget.add(self, widget)
        if self._slave:
            self._slave.kill()
        if self.orientation:
            self.links.add(widget)
        self._slave = widget
        self.update_geometry()

    def remove(self, widget):
        """Loescht das uebergebenen Widget aus dem Container."""

        if widget == self._slave:
            self._slave = None
        self.kill()

    def mouse_test(self, pos):
        """Prueft die Position im Container, und in den Widgets."""
        try:
            Widget.mouse_test(self, pos)
        except MouseIn:
            if not self.event_master:
                self._slave.mouse_test(self.mouse_pos)
            raise

    def frame_mouse_test(self, pos):
        """Prueft die Position im Container, und in den Widgets."""
        try:
            Widget.mouse_test(self, pos)
        except MouseIn:
            if not self.event_master:
                pos = (self.mouse_pos[0] - self._frame,
                       self.mouse_pos[1] - self._frame)
                self._slave.mouse_test(pos)
            raise

    def clean(self):
        """Loescht alle Widget."""

        self._slave.kill()
