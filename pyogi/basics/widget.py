from .event import Event
from .focus import Focus
from pyogi.geometry.geometry import Geometry


class Widget(Event, Focus, Geometry):

    def __init__(self, visibility=True, **config):
        """Basis aller Widget"""
        Event.__init__(self)
        Geometry.__init__(self)

        self.visibility = visibility  # bestimmt ob Sichtbar oder nicht
        self.alive = False
        self.changed = False
        self._config = config
        self._theme = None
        self._master = None

    def create(self):
        self._theme = self._config.get("theme", self._master._theme)
        self.alive = True
        self.update_geometry()

    def config(self, **values):
        self._config.update(values)
        if self.alive:
            self.create()

    def remove_config(self, *values):
        hit = False
        for key in values:
            if key in self._config:
                hit = True
                del self._config[key]
        if hit:
            self.create()

    def update(self):
        pass

    def composit(self, composer):
        composer.create(self)
        self.draw = composer
        self.change()

    def remove_composer(self, event=None):
        try:
            self.draw.kill()
            self.change()
        except AttributeError:
            pass

    def draw(self, buffer):
        if self.changed:
            self.update()
            self.changed = False
        if self.visibility:
            buffer.blit(self._image, self.geometry)

    def toggle_visibility(self):
        """Wechselt zwischen sichtbar und unsichtbar."""

        self.visibility = False if self.visibility else True
        if self._master:
            self._master.change()

    def change(self):
        """Gibt das aktuelle Widget zum update frei."""
        if not self.changed and self.alive:
            try:
                self._master.change()
            except:
                print(self)
                raise
            self.changed = True

    def remove(self, widget):
        pass

    def add(self, widget):

        if widget._master:
            widget._master.remove(widget)
        widget._master = self
        if self.alive:
            widget.create()##############

    def kill(self):
        """Loescht das Widget vom Master."""

        if self._master:
            if self == Focus.active:
                Focus.active = None
            self.alive = False
            self.changed = False
            if self in self._master.links:
                self._master.links.remove(self)
            self.kill_link()
            #self.links.clear()
            #self.kill_link()
            self._master.remove(self)
