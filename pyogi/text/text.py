import pygame

from pyogi.basics import SingleContainer, Widget
from pyogi.text.base import Text
from pyogi.text.multiline import Multiline
from pyogi.locals import *


class Share(SingleContainer):

    def __init__(self, widget, size="auto", orientation="center", **config):
        SingleContainer.__init__(self, size, orientation, **config)
        self.event_master = True
        self.add(widget)

    def create(self):
        SingleContainer.create(self)
        self._color = self._config.get("widget_bg",
                                       self._theme.colors.widget_bg)

    def update(self):

        SingleContainer.update(self)
        try:
            self._color[3]
            self._image = pygame.Surface(self.size).convert_alpha()
        except IndexError:
            self._image = pygame.Surface(self.size)
        self._image.fill(self._color)
        SingleContainer.draw(self, self._image)

    @property
    def text(self):
        return self._slave.text

    @text.setter
    def text(self, value):
        self._slave.text = value
        self.update_link()

    draw = Widget.draw


class Label(Share):

    def __init__(self, text, font, size="auto", orientation="center", **config):
        """Einzeiliges Text Widget auf einer Farbe abgebildet"""

        Share.__init__(self, Text(text, font, **config),
                       size, orientation, **config)

        self.bind_event_mode(MOUSEBUTTONDOWN, 4, lambda: self.xscroll(10))
        self.bind_event_mode(MOUSEBUTTONDOWN, 5, lambda: self.xscroll(-10))

    @property
    def font(self):
        return self._slave.font

    @font.setter
    def font(self, font):
        self._slave.font = font
        self.reset_scroll()
        self.change()


class Textbox(Share):

    def __init__(self, text, font, size="auto", from_side="topleft",
                 to_side="bottomleft", offset=(0, 0), **config):
        """Mehrzeiliges Text Widget auf einer Farbe abgebildet"""

        Share.__init__(self, Multiline(text, font, from_side=from_side,
                                       to_side=to_side, offset=offset,
                                       **config),
                       size, from_side, **config)

        self._from_side = from_side
        self._to_side = to_side
