import pygame

from pyogi.basics import Widget


class  Text(Widget):

    def __init__(self, text, font, **config):
        """Einzeiliges Text Widget auf einer Farbe abgebildet"""

        Widget.__init__(self, **config)

        self._text = text
        self._font = font
        self.geometry = pygame.Rect((0, 0), font.get_rect(text).size)

    def create(self):
        Widget.create(self)
        self._fg = self._config.get("text_fg", self._theme.colors.text_fg)
        self._bg = self._config.get("text_bg", self._theme.colors.text_bg)

    def draw(self, buffer):
        """zeichnet auf das uebergebene Surface"""

        if self.visibility:
            self.changed = False
            buffer.blit(self._font.render(self.text, self._fg, self._bg)[0],
                        self.geometry)

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value
        self.size = self._font.get_rect(value).size

    @property
    def font(self):
        return self._font

    @font.setter
    def font(self, value):
        self._font = value
        self.size = self._font.get_rect(value).size

    def __len__(self):
        return len(self._text)

    def __bool__(self):
        return True

    def get_surface(self):
        """giebt den text als Surface zurueck"""

        return self.font.render(self.text, self._fg, self._bg)[0]
