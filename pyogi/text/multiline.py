import pygame

from pyogi.basics import Widget
from pyogi.basics.event import MouseIn
from pyogi.text.base import Text
from pyogi.geometry import sort, sizes


class Multiline(Widget):

    def __init__(self, text, font, from_side="topleft", to_side="bottomleft",
                 offset=(0, 0), **config):
        """Mehrzeiliger Text Widget"""

        Widget.__init__(self, **config)

        self.font = font
        self._text = text
        self._from_side = from_side
        self._to_side = to_side
        self._offset = offset
        self.geometry = pygame.Rect((0, 0), (0, 0))
        self.text_list = self.create_text_list(self._text.splitlines())

    def clar(self):
        for i in self.text_list:
            i.kill()
        self.geometry = pygame.Rect((0, 0), (0, 0))
        self.text_list = []

    def create(self):

        Widget.create(self)
        for widget in self.text_list:
            widget.create()

    def update(self):

        if self.text_list:
            sort.row(self.text_list, startpos=self.get_side(self._from_side),
                     offset=self._offset, from_side=self._from_side,
                     to_side=self._to_side)
            self.geometry = sizes.bound_rect(self.text_list)

    def draw(self, buffer):

        if self.changed:
            self.update()
            self.changed = False
        if self.visibility:
            for text in self.text_list:
                text.draw(buffer)

    def create_text_list(self, liste):
        """Macht aus einer liste Strings eine liste mit Base_text Widget"""

        text_list = []
        if liste:
            for text in liste:
                widget = Text(text, self.font, **self._config)
                self.add(widget)
                text_list.append(widget)
            sort.row(text_list, startpos=self.get_side(self._from_side),
                     offset=self._offset, from_side=self._from_side,
                     to_side=self._to_side)
            self.geometry = sizes.bound_rect(text_list)
        else:
            self.geometry = pygame.Rect((0, 0), (0, 0))
        return text_list

    @property
    def text(self):

        return self._text

    @text.setter
    def text(self, value):

        self._text = value
        self.text_list = self.create_text_list(self._text.splitlines())
        self.change()

    def add_row(self, text):
        """Fuegt ans ende eine neue Zeile ein"""

        new_text = Text(text, self.font, **self._config)
        self._text = "%s\n%s" % (self._text, text)
        self.text_list.append(new_text)
        self.add(new_text)
        if self._theme:
            new_text.create()
        sort.row(self.text_list, startpos=self.get_side(self._from_side),
                        offset=self._offset, from_side=self._from_side,
                        to_side=self._to_side)
        self.geometry = sizes.bound_rect(self.text_list)
        self.change()
        return new_text

    def remove(self, widget):
        """Loescht das uebergebenen Widget aus dem Container."""

        self.text_list.remove(widget)
        self.change()

    def mouse_test(self, pos):
        """testet ob pos in Widget liegt"""

        try:
            Widget.mouse_test(self, pos)
        except MouseIn:
            for text in self.text_list:
                text.mouse_test(pos)
