
import pygame

import pyogi
from pyogi.composit import Base
from pyogi.text import Label
from pyogi.locals import *

# TODO: änder scrollen beim schreiben

class Cursor(Base):

    def __init__(self, widget, color=(255, 255, 255)):
        self.color = color
        self._pos = (0, 0)
        self.visibility = False
        self.widget = widget

    @property
    def pos(self):
        return (self._pos[0] + self.widget.x, self._pos[1] + self.widget.y)

    @pos.setter
    def pos(self, pos):
        self._pos = pos

    def reset(self, event=None):
        self.visibility = True

    def switch_visibility(self, event=None):
        self.visibility = not self.visibility
        self.widget.change()

    def __call__(self, surface):
        self.draw(surface)
        if self.visibility:
            pos = self.pos
            pygame.draw.line(surface, self.color, pos,
                             (pos[0], pos[1] + self.widget.h), 1)


class Entry(Label):

    def __init__(self, font, size=(64, 16), event=None, orentation="center",
                 input_typ=str, scrolable=False, writable=True, **config):

        Label.__init__(self, "", font, size, orentation, **config)

        self.scrolable = scrolable
        self.event = event
        self.input_typ = input_typ
        self._cursor = Cursor(self)
        self._cursorpos = 0
        self.last_input = ""
        self.writable = writable
        self.write_time = pyogi.event.InputDelayChek(600)
        self.cursor_time = pyogi.event.Delay(self._cursor.switch_visibility,
                                             600)
        self.bind_event_mode(MOUSEBUTTONUP, 1, self.set_cursor)
        self.bind_event_mode(MOUSEBUTTONUP, 4, self.move_cursor)
        self.bind_event_mode(MOUSEBUTTONUP, 5, lambda event: self.move_cursor(-1))
        self.bind_event(SET_FOCUS, self.activate)
        self.bind_event(LOST_FOCUS, self.disable)
        self.bind_event(FOCUS, self.cursor_time)
        self.bind_event(KEYDOWN, self.tiping)
        self.bind_event(KEYUP, self.write_time.reset)
        self.bind_event_mode(KEYDOWN, K_BACKSPACE,
                             lambda event: self.write_time(self.del_cursor))
        self.bind_event_mode(KEYDOWN, K_LEFT, lambda event: self.write_time(
                             lambda event: self.move_cursor(-1, event=event), event)
                            )
        self.bind_event_mode(KEYDOWN, K_RIGHT, lambda event: self.write_time(
                             self.move_cursor, event=event)
                            )
        self.bind_event((1, 0, 0), self.set_cursor)

    def create(self):
        Label.create(self)
        self._cursor.color = self._config.get("text_fg",
                                              self._theme.colors.text_fg)

    def get(self):
        try:
            return self.input_typ(self._slave.text)
        except ValueError:
            self.text = "0"
            return self.input_typ("0")

    @property
    def cursorpos(self):
        return self._cursorpos

    @cursorpos.setter
    def cursorpos(self, pos):
        self._cursorpos = pos
        self.cursor_time.reset()
        self._cursor.reset()
        self.change()

    def activate(self, event=None):
        self.slave.composit(self._cursor)

    def disable(self, event=None):
        self.slave.remove_composer()

    def write(self, text):
        new_text = "%s%s%s" % (self.text[:self.cursorpos], text,
                               self.text[self.cursorpos:])
        try:
            self.input_typ(new_text)
        except ValueError:
            return
        if self.font.get_rect(new_text).w < self.w or self.scrolable:
            self.text = new_text
            if self.event:
                self.event(new_text)
            self.move_cursor(len(text))

    def del_cursor(self, event=None):
        if self.cursorpos > 0:
            self.move_cursor(-1)
            self.text = self.text[:self.cursorpos] + self.text[self.cursorpos+1:]
        if self.event:
            self.event(self.text)

    def clear(self):
        if self.text:
            self.text = ""
            self.cursorpos = 0
            if self.event:
                self.event(self.text)

    def tiping(self, event):
        if event.unicode and self.slave.font.get_metrics(event.unicode)[0]:
            self.write_time(self.write, event.unicode)

    def set_cursor(self, event=None):
        if self.writable:
            if self.text:
                if self.mouse_pos[0] < self.slave.x:
                    self.cursorpos = 0
                    self.focussing()
                    self._cursor.pos = (0, 0)
                    return
                elif self.mouse_pos[0] > self.slave.x + self.slave.w:
                    self.cursorpos = len(self.text)
                    self.focussing()
                    self._cursor.pos = (self.slave.w, 0)
                    return
                pos_x = self.mouse_pos[0] - self.slave.x
                cursorpos = 0
                width = 0
                for letter_size in self.font.get_metrics(self.text):
                    half_width = letter_size[4] / 2
                    width += half_width
                    if pos_x < width:
                        self.cursorpos = cursorpos
                        self.focussing()
                        self._cursor.pos = (width - half_width, 0)
                        return
                    width += half_width
                    cursorpos += 1
                    if pos_x < width:
                        self.cursorpos = cursorpos
                        self.focussing()
                        self._cursor.pos = (width, 0)
                        return
            else:
                self.cursorpos = 0
            self.focussing()

    def move_cursor(self, value=1,  event=None):
        new_cursorpos = self.cursorpos + value
#################### xscrool falsch
        if 0 > new_cursorpos or new_cursorpos > len(self.text):
            return
        self.cursorpos = new_cursorpos
        pos_x = self.font.get_rect(self.text[:self.cursorpos]).w
        if value > 0:
            #self.xscroll(self.w - pos_x)
            self.set_xscroll(-pos_x + self.w)
        else:
            self.set_xscroll(-pos_x + self.w)
        self._cursor.pos = (pos_x, 0)
        self.focussing()
