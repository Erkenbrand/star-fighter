from pyogi.locals import REFLECT_SIDE


class Side:

    def __init__(self, link, side, anchor, offset):
        self.link = link
        self.side = side
        self.anchor = anchor
        self.offset = offset

    def kill(self, widget):
        self.link.links.remove(widget)

    def __call__(self, widget):
        if self.link == widget._master:
            new_pos = self.link.local_side(self.anchor)
        else:
            new_pos = self.link.get_side(self.anchor)
        new_pos = (new_pos[0] + self.offset[0], new_pos[1] + self.offset[1])
        setattr(widget.geometry, self.side, new_pos)


class Size:

    def __init__(self, link, link2, anchor, anchor2, offset, offset2,
                 mode="size"):
        self.link = link
        self.anchor = anchor
        self.offset = offset
        self.link2 = link2
        self.anchor2 = anchor2
        self.offset2 = offset2
        self.mode = mode

    def kill(self, widget):
        self.link.links.remove(widget)
        if self.link !=self.link2:
            self.link2.links.remove(widget)

    def __call__(self, widget):
        if self.link == widget._master:
            side = self.link.local_side(self.anchor)
        else:
            side = self.link.get_side(self.anchor)
        side = (side[0] + self.offset[0], side[1] + self.offset[1])

        if self.link2 == widget._master:
            side2 = self.link2.local_side(self.anchor2)
            side2 = (side2[0] + self.offset2[0], side2[1] + self.offset2[1])
            pos = side2
        else:
            side2 = self.link2.get_side(self.anchor2)
            side2 = (side2[0] + self.offset2[0], side2[1] + self.offset2[1])
            pos = side

        if self.mode == "size":
            size = (abs(side[0] - side2[0]), abs(side[1] - side2[1]))
            setattr(widget.geometry, "size", size)
            center = ((side2[0] - side[0]) / 2 + side[0],
                  (side2[1] - side[1]) / 2 + side[1])
            setattr(widget.geometry, "center", center)
        elif self.mode == "w":
            w = abs(side[0] - side2[0])
            x = side[0] + w / 2
            setattr(widget.geometry, "w", w)
            setattr(widget.geometry, self.anchor2, pos)
        elif self.mode == "h":
            h = abs(side[1] - side2[1])
            y = side[1] + h / 2
            setattr(widget.geometry, "h", h)
            if self.link == widget._master:
                setattr(widget.geometry, self.anchor, side)
            else:
                setattr(widget.geometry, self.anchor, side2)


class Link:

    def __init__(self):
        self._link = None
        self.links = set()

    def kill_link(self):
        if self._link:
            self._link.kill(self)

    def link_side(self, widget, side="topleft", anchor="bottomright",
                  offset=(0, 0)):
        """Bindet das Widge, an den uebergebennen link."""
        widget.links.add(self)
        self._link = Side(widget, side, anchor, offset)
        self.update_link()

    def link_size(self, widget, widget2, anchor, anchor2, offset=(0, 0),
                  offset2=(0, 0), mode="size"):
        """Bindet das Widge, an den uebergebennen link."""
        widget.links.add(self)
        widget2.links.add(self)
        self._link = Size(widget, widget2, anchor, anchor2, offset, offset2,
                          mode)
        self.update_link()

    def update_link(self):
        if self._link:
            self._link(self)
        for link in self.links:
            link.update_link()
        self.change()
