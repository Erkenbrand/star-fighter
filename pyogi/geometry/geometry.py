from pyogi.locals import REFLECT_SIDE
from pyogi.basics.event import MouseIn
from pyogi.geometry.link import Link


class Geometry(Link):

    def __init__(self):
        Link.__init__(self)
        self._size = None
        self._pos = None
        self.mouse_pos = (0, 0)

    def update_geometry(self):
        if self._master:
            if self._size == "auto":
                self.autosize()
            elif self._size == "fill" and self.alive:
                self.fill()
            self.update_link()
            if self._master._size == "auto":
                self._master.update_geometry()
            self.change()
        elif self._size == "auto":
            self.autosize()
        ######!!!!!!!

    def autosize(self):
        pass

    @property
    def size(self):
        return self.geometry.size

    @size.setter
    def size(self, value):
        try:
            self.geometry.size = value
        except TypeError:
            self._size = value
        self.update_geometry()

    def fill(self):
        self.geometry.size = self._master.fill_size

    @property
    def fill_size(self):
        """Gibt die Groesse, die von einem widget befuellt werden kann zurueck.
        """

        return self.geometry.size

    @property
    def pos(self):
        return self.geometry.topleft

    @pos.setter
    def pos(self, value):
        self.geometry.topleft = value

    @property
    def w(self):
        return self.geometry.w

    @w.setter
    def w(self, value):
        self.size = (value, self.h)

    @property
    def h(self):
        return self.geometry.h

    @h.setter
    def h(self, value):
        self.size = (self.w, value)

    @property
    def x(self):
        return self.geometry.x

    @x.setter
    def x(self, value):
        self.geometry.x = value

    @property
    def y(self):
        return self.geometry.y

    @y.setter
    def y(self, value):
        self.geometry.y = value

    def get_side(self, side):
        """Gibt die Position des Widget ueber eine Seite aus."""
        return getattr(self.geometry, side)

    def set_side(self, pos, side):
        """Setzt die Position des Widget ueber die angegebene Seite."""
        setattr(self.geometry, side, pos)
        self.update_geometry()

    def global_side(self, side):
        """Gieb die seite mit globaler positon aus."""
        return self._master.global_pos(self.get_side(side))

    def local_side(self, side):
        """Gieb die seite im widget zuruek."""
        return self.local_master_pos(self.get_side(side))

    def global_master_pos(self, pos):
        """Gibt die Position, im widget, auf master Ebene zurueck."""
        return (pos[0] + self.x, pos[1] + self.y)

    def global_pos(self, pos, max_dep=None):
        """Gibt die Position, im widget, auf globaler Ebene oder bis max_dep
        zurueck.
        """
        if max_dep == self:
            return pos
        return self._master.global_pos(self.global_master_pos(pos), max_dep)

    def local_master_pos(self, pos):
        """Gibt die Position, im widget, auf master Ebene zurueck."""
        return (pos[0] - self.x, pos[1] - self.y)

    def local_pos(self, pos, start=None):
        """Gibt die Position, im widget, auf lokaler Ebene oder bis start
        zurueck.
        """
        pos = self.local_master_pos(pos)
        if start == self:
            return pos
        return self._master.local_pos(pos)

    def move(self, way=(0, 0)):
        """Bewegt das Widget auf dem Master um den angegebenen Weg."""
        self.geometry.move_ip(way)
        self.update_geometry()

    def move_in(self, geometry, way=(0, 0)):
        """
        Bewegt das Widget auf dem master um den angegebenen Weg,
        solange es in der uebergebenen geometry ist.
        Ist es nicht mehr in der geometry, wird False zurueckgegeben,
        (ansonsten True) und das Widget wird nicht bewegt.
        """
        new = self.geometry.move_ip(way)
        if geometry.contains(new):
            self.geometry = new
            self.change()
            return True
        else:
            return False

    def move_in_master(self, way=(0, 0)):
        """
        Bewegt das Widget auf dem master um den angegebenen weg,
        solange es im master ist.
        Ist es nicht mehr im master, wird False zurueckgegeben, (ansonsten True)
        und das Widget wird nicht bewegt.
        """
        return self.move_in(self._master.geometry, way)

    def mouse_test(self, pos):
        """Testet ob pos im Widget liegt."""
        if self.visibility and self.geometry.collidepoint(pos):
            self.mouse_pos = self.local_master_pos(pos)
            MouseIn.add(self)
            raise MouseIn
