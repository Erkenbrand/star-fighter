import math

import pygame

from pyogi.ordinary.container import BaseContainer


class Gride(BaseContainer):

    def __init__(self, size="fill", scaling="fill", **config):
        """Einfaches Gride Widget, das die mit add hinzugefuegten Widget,
        in einem Raster anordnet."""

        BaseContainer.__init__(self, **config)

        self.geometry = pygame.Rect((0, 0), (0, 0))
        self.size = size
        self._scaling = scaling

    @property
    def scaling(self):
        return self._scaling

    @scaling.setter
    def scaling(self, value):
        self._scaling = value
        self.change()

    def update(self):
        if not self._slaves: return
        posts = len(self._slaves)
        num = math.sqrt(posts)
        inum = int(num)
        positive = math.ceil(num)
        width = self.w / positive
        rest = posts % positive
        if rest:
            rest_width = self.w / rest
        else:
            rest_width = width
        if num - 0.5 < inum:
            height = self.h / inum
        else:
            height = self.h / positive
        count = 0
        row = 0
        for slave in self._slaves:
            if self.scaling == "fill":
                slave.size = (abs(width), abs(height))
                slave.pos = (self.x + width * count, self.y + height * row)
            else:
                slave.pos = (self.x + width * count + width / 2,
                             self.y + height * row + height / 2)
            count += 1
            if count == positive:
                count = 0
                row += 1
                if row+1 == int(num+0.5):
                    width = rest_width

    def draw(self, buffer):
        if self.changed:
            self.update()
            self.changed = False
        BaseContainer.draw(self, buffer)
