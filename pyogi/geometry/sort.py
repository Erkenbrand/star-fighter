def row(widgets, startpos=(0, 0), from_side="midtop",
        to_side="midbottom", offset=(0, 0)):
    if widgets:
        last = widgets[0]
        last.set_side(startpos, from_side)
        for widget in widgets[1:]:
            pos = last.get_side(to_side)
            pos = (pos[0] + offset[0], pos[1] + offset[1])
            widget.set_side(pos, from_side)
            last = widget


class Row:

    def __init__(self, from_side="midtop", to_side="midbottom", offset=(0, 0)):
        self.offset = offset
        self.from_side = from_side
        self.to_side = to_side

    def __call__(self, widgets, master):
        # TODO: nicht bei composit
        pos = master.local_side(self.from_side)
        for widget in widgets:
            setattr(widget.geometry, self.from_side, pos)
            pos = widget.get_side(self.to_side)
            pos = (pos[0] + self.offset[0], pos[1] + self.offset[1])
