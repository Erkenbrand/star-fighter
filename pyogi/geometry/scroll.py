def scroll_y(self, y=0):
    """Scrollt um y."""

    if y < 0 :
        array = self.get_scroll()[1]
        if 0 < array[1] > -y:
            self.scroll_pos_y += y
        else:
            self.scroll_pos_y -= array[1]
        self.change()
    elif y > 0 :
        array = self.get_scroll()[1]
        if 0 > array[0] < -y:
            self.scroll_pos_y += y
        else:
            self.scroll_pos_y -= array[0]
        self.change()

def scroll_x(self, x=0):
    """Scrollt um x."""

    if x < 0 :
        array = self.get_scroll()[0]
        if 0 < array[1] > -x:
            self.scroll_pos_x += x
        else:
            self.scroll_pos_x -= array[1]
        self.change()
    elif x > 0 :
        array = self.get_scroll()[0]
        if 0 > array[0] < -x:
            self.scroll_pos_x += x
        else:
            self.scroll_pos_x -= array[0]
        self.change()