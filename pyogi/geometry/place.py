from pyogi.locals import REFLECT_SIDE


def to(brother, widget, side="midtop", anchor=None, offset=(0, 0), link=False):
    """Platziert ein Widget an ein anderes (brother).

    brother : Giebt das Widget an, an das angeheftet wird.
    side : Gibt die Seite des Widget an, die angeheftet wird.
    anchor : Gibt die Seite an, an die das Widget angeheftet wird.
    offset : Wird zur Position hinzugefuegt.-
    """
    if brother._master:
        brother._master.add(widget)
        if not anchor:
            anchor = REFLECT_SIDE[side]
        if link:
            widget.link_side(brother, side, anchor, offset)
        else:
            x, y = brother.get_side(anchor)
            widget.set_side((offset[0] + x, offset[1] + y), side)


def in_row(container, widgetlist, pos=(0, 0), side="topleft", anchor=None,
           from_side="topleft", to_side="bottomleft", offset=(0, 0),
           link=False):
    """Platziert eine liste von Widgets in eine reihen folge aneinander.

    side : Gibt die Seite an, von der das erste Widget angeheftet wird.
    anchor : Gibt die Seite an, an die das erste Widget angeheftet wird.
    from_side : Gibt die Seite an, von der in reihe angeheftet wird.
    too_side : Gibt die Seite an, an die in reihe angeheftet wird.
    """
    last = widgetlist[0]
    container.place(last, pos, side, anchor, link=link)
    for widget in widgetlist[1:]:
        to(last, widget, from_side, to_side, offset, link)
        last = widget
