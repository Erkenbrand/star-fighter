import pygame

from pyogi.basics.widget import Widget


class BaseColor(Widget):

    def __init__(self, size=(10, 10), color=(125,0,0), width=0):
        """Einfaches einfarbiges Widget."""

        Widget.__init__(self)

        self.geometry = pygame.Rect((0, 0), size)
        self._color = color
        self._width = width

    @property
    def color(self):

        return self._color

    @color.setter
    def color(self, value):

        self._color = value
        self.change()

    def draw(self, buffer):
        if self.visibility:
            pygame.draw.rect(buffer, self._color, self.geometry, self._width)
