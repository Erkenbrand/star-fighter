def correct(value):
    """korrigiert einen Farbwert auf einen zulaessigen wert"""

    if value > 255:
        return 255
    elif value < 0:
        return 0
    else:
        return value

def normalize(color):
    """korrigiert alle Farben auf einen zulaessigen wert"""

    r, g, b = color
    return (correct(r), correct(g), correct(b))