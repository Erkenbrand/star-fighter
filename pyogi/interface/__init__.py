import pygame

import pyogi
from pyogi.locals import *
from pyogi.basics import Focus
from pyogi.basics.event import MouseIn


class Gui:

    dialog = None
    popup = None
    timer = set()

    def __init__(self, size=(600, 600), theme=DEFAULT_THEME):
        """Beinhaltet alle anderen Widget"""
        Gui.instance = self
        self._theme = theme
        self._slaves = []
        self._size = size
        self.geometry = pygame.Rect((0, 0), size)
        self.links = set()
        self.buttons = (0, 0, 0)
        self.mouse_pos = pygame.mouse.get_pos()
        self.changed = True
        self._frame = None
        self.mouse_testet = False
        self.blok = False
        self.events = {MOUSEMOTION: self.mouse_motion,
                       MOUSEBUTTONDOWN: self.mouse_input,
                       MOUSEBUTTONUP: self.mouse_input,
                       KEYDOWN: self.key_input,
                       KEYUP: self.key_input,
                       VIDEORESIZE: self.resize}
        MouseIn.add(self)

    def config(self, **values):

        self._theme = values.get("theme", DEFAULT_THEME)
        for widget in self._slaves:
            widget.config()

    @property
    def x(self):
        return self.geometry.x

    @property
    def y(self):
        return self.geometry.y

    def play_event(self, name, event):
        """None Methode."""
        pass

    def test_event(self, event):

        event_funk = self.events.get(event.type, None)
        if event_funk:
            event_funk(event)
        else:
            print(event, "nicht vorhanden")

    def mouse_motion(self, event):
        """
        Speichert die aktuelle Mausposition und ruft dann einen mouse_test auf,
        in dem das Event (MOUSEMOTION - buttons) uebergeben wird.
        """
        self.mouse_pos = event.pos
        self.buttons = event.buttons
        self.mouse_test(event)
        MouseIn.widget.play_event(EVENTSTRING % (MOUSEMOTION, self.buttons),
                                  event)
        MouseIn.widget.play_event(MOUSEMOTION, event)

    def mouse_test(self, event=None):
        """Testet in welchen Widget die Maus ist.

        ist die Maus zum ersten mal im Widget dann wird das Event MOUSE_GO_IN
        und (MOUSE_GO_IN - buttons) aufgerufen. verlaest die Maus ein Widget,
        dann wird das Event MOUSE_GO_OUT aufgerufen.
        In dem Widget wo die Maus ist wird ein Event buttons aufgerufen z.b. (1,0,0)
        """
        MouseIn.add(self)
        try:
            if self.dialog:
                self.dialog.mouse_test(self.mouse_pos)
            for widget in reversed(self._slaves):
                widget.mouse_test(self.mouse_pos)
            raise MouseIn
        except MouseIn:
            for widget in MouseIn.old_in - MouseIn.new_in:
                widget.play_event(MOUSE_GO_OUT, event)
            for widget in MouseIn.new_in - MouseIn.old_in:
                widget.play_event(MOUSE_GO_IN, event)
                widget.play_event(EVENTSTRING % (MOUSE_GO_IN, self.buttons),
                                  event)
            MouseIn.old_in = MouseIn.new_in
            MouseIn.new_in = set()
        self.mouse_testet = True

    def mouse_input(self, event):
        """Vuert ein Maus Event aus.
        name : MOUSEBUTTONUP, MOUSEBUTTONDOWN
        button : 1, 2, 3, 4, 5
        ruft mouse_test auf
        """
        # IDEA: ersetze EVENTSTRING mit pygame.event.Event
        name = EVENTSTRING % (event.type, event.button)
        self.buttons = pygame.mouse.get_pressed()
        self.mouse_test(event)
        MouseIn.widget.play_event(name, event)
        MouseIn.widget.play_event(event.type, event)

    def update(self, event=None):
        """Fuert das FOCUS event aus"""
        for timer in Gui.timer.copy():
            timer()
        if self.mouse_testet:
            self.mouse_test(event)
        MouseIn.widget.play_event(self.buttons, None)
        MouseIn.widget.play_event(MOUSE_IN, None)
        Focus.update()
        self.mouse_testet = False

    def change(self):
        self.changed = True

    def key_input(self, event):
        """Vuert eine tastertureingabe aus.

        mod : pygame events KEYDOWN, KEYUP
        key : die nummer der Taste
        unicode : Taste als Schriftzeichen
        """
        key = event.key
        if Focus.active:
            Focus.active.play_event(EVENTSTRING % (event.type, key), event)
            Focus.active.play_event(event.type, event)

    def global_pos(self, pos, max_dep=None):
        """gibt die Position von Pos im Widget zurueck."""
        return pos

    def local_pos(self, pos, start=None):
        """gibt die Position von Pos im Widget zurueck."""
        return pos

    def resize(self, event):
        """Setzt die Groesse des Gui neu"""
        self.geometry.size = event.size
        self.update_link()

    def update_link(self):
        for link in self.links:
            link.update_link()
        self.change()

    @property
    def fill_size(self):
        """
        Gibt die groesse, die von einem widget befuellt werden kann zurueck.
        """
        return self.geometry.size

    def place(self, widget, pos=(0, 0), side="topleft", anchor=None,
              link=False):
        """platziert das Widget an der uebergebenen Position
        side : gibt die Seite an die platziert wird
        anchor : gibt die Seite des Containers an,
        von der aus das Widget platziert wird
        """
        self.add(widget)
        if not anchor:
            anchor = side
        if link:
            widget.link_side(self, side, anchor, pos)
        else:
            x, y = self.local_side(anchor)
            widget.set_side((pos[0] + x, pos[1] + y), side)

    def add(self, widget):
        """Fuegt das widget dem Gui hinzu"""
        if widget._master:
            widget._master.remove(widget)
        widget._master = self
        widget.create()
        self._slaves.append(widget)
        self.change()

    def add_all(self, widgets):
        for widget in widgets:
            self.add(widget)

    def remove(self, widget):
        """Loescht das uebergebenen Widget aus dem Container."""
        self._slaves.remove(widget)
        widget._master = None
        self.update_link()

    def get_side(self, side):
        """Gibt die Position des Gui ueber eine Seite aus."""
        return getattr(self.geometry, side)

    def local_side(self, side):
        """Gibt die Position des Gui ueber eine Seite aus."""
        return getattr(self.geometry, side)

    def bloking(self):
        self.blok = True

    def draw(self, buffer):
        """Zeichnet alle gui Elemente auf das uebergebene Surface."""
        for widget in self._slaves:
            widget.draw(buffer)
        if Gui.dialog:
            Gui.dialog.draw(buffer)
        if Gui.popup:
            Gui.popup.draw(buffer)
        if self.blok:
            self.blok = False
        else:
            self.changed = False

    def clean(self):
        """Loescht alle Widget."""
        for widget in self._slaves[:]:
            widget.kill()
