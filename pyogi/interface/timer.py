from pyogi.basics.event import Delay
from . import Gui


def add(event, time=1000, once=False):
    if once:
        Gui.timer.add(Delay(event, time, Gui.timer.discard))
    else:
        timer = Delay(event, time)
        Gui.timer.add(timer)
        return timer


def remove(timer):
    Gui.timer.discard(timer)


def clean():
    Gui.timer.clear()
