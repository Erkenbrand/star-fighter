from . import Gui

def set(widget, pos, side="topleft"):
    Gui.popup = widget
    Gui.popup_side = side
    widget._master = Gui.instance
    widget.create()
    widget.set_side(pos, side)

def move(pos):
    Gui.popup.set_side(pos, Gui.popup_side)

def clean():
    if Gui.popup:
        Gui.popup._master = None
        Gui.popup.alive = False
        Gui.popup.changed = False
        Gui.popup = None
        Gui.instance.change()
