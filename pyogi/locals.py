﻿from pygame.locals import *

from pyogi.theme import base

EVENTSTRING = "%s-%s"
MOUSE_GO_OUT = "mouse_go_out"
MOUSE_GO_IN = "mouse_go_in"
MOUSE_IN  = "mouse_in"
SET_FOCUS = "set_focus"
LOST_FOCUS = "lost_focus"
FOCUS = "focus"
REFLECT_SIDE = {"topleft":"bottomright", "midtop":"midbottom",
             "topright":"bottomleft", "midleft":"midright", "center":"center",
             "midright":"midleft", "bottomleft":"topright",
             "midbottom":"midtop", "bottomright":"topleft"
             }
DEFAULT_THEME = base