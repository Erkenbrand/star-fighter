
class Base:

    def create(self, widget):
        self.widget = widget
        self.draw = widget.draw

    def kill(self):
        self.widget.draw = self.draw
        self.widget.change()
        self.draw = None
        self.widget = None
