import pygame

from pyogi.composit import Base


class Frame(Base):

    def __init__(self, frame=1, collor=0xff0000):
        self.frame = frame
        self.collor = collor

    def __call__(self, surface):
        self.draw(surface)
        pygame.draw.rect(surface, self.collor, self.widget.geometry, self.frame)
