import pygame

def dashed(color, image):
    size = image.get_size()
    last_x = 0
    last_y = 0
    for w in range(size[0] // 8, size[0], 4):
        pygame.draw.line(image, color, (last_x, 0), (last_x + 3, 0))

        pygame.draw.line(image, color,
                         (last_x, size[1] - 1), (last_x + 3, size[1] - 1))
        last_x += 8
    for y in range(size[1] // 8):
        pygame.draw.line(image, color, (0, last_y + 3), (0, last_y + 6))

        pygame.draw.line(image, color,
                         (size[0] - 1, last_y + 3), (size[0] - 1, last_y + 6))
        last_y += 8

    return image