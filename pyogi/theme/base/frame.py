import math

import pygame

from pyogi.color.edit import normalize

def down(image, color, height):

    size = image.get_size()
    add = 200 / (height*2)
    heel_color = normalize((color[0] + add, color[1] + add, color[2] + add))
    pygame.draw.line(image, heel_color, (0, 0), (size[0] - 1, 0))
    pygame.draw.line(image, heel_color, (size[0] - 1, 0),
                     (size[0] - 1, size[1] - 1))
    pygame.draw.line(image, heel_color, (size[0] - 1, size[1] - 1),
                     (0, size[1] - 1))
    pygame.draw.line(image, heel_color, (0, size[1] - 1), (0, 0))
    if height > 1:
        for h in range(height-1):
            h += 1
            color = normalize((color[0] - add, color[1] - add, color[2] - add))
            pygame.draw.line(image, color, (h, h), (size[0] - 1 - h, h))
            pygame.draw.line(image, color, (size[0] - 1 - h, h),
                             (size[0] - 1 - h, size[1] - 1 - h))
            pygame.draw.line(image, color, (size[0] - 1 - h, size[1] - 1 - h),
                             (h, size[1] - 1 - h))
            pygame.draw.line(image, color, (h, size[1] - 1 - h), (h, h))

def up(image, color, height):

    size = image.get_size()
    add = 200 / height
    dark_color = normalize((color[0] - add, color[1] - add, color[2] - add))
    pygame.draw.line(image, dark_color, (height - 1, height - 1),
                     (size[0] - height, height - 1))
    pygame.draw.line(image, dark_color, (size[0] - height, height - 1),
                     (size[0] - height, size[1] - height))
    pygame.draw.line(image, dark_color, (size[0] - height, size[1] - height),
                     (height - 1 , size[1] - height))
    pygame.draw.line(image, dark_color, (height - 1, size[1] - height),
                     (height - 1, height - 1))
    color = normalize((color[0] - add * height / 2, color[1] - add * height / 2,
                      color[2] - add * height / 2))
    if height > 1:
        for h in range(math.ceil(height / 2)):
            color = normalize((color[0] + add, color[1] + add, color[2] + add))
            pygame.draw.line(image, color, (h, h), (size[0] - 1 - h, h))
            pygame.draw.line(image, color, (size[0] - 1 - h, h),
                             (size[0] - 1 - h, size[1] - 1 - h))
            pygame.draw.line(image, color, (size[0] - 1 - h, size[1] - 1 - h),
                             (h, size[1] - 1 - h))
            pygame.draw.line(image, color, (h, size[1] - 1 - h), (h, h))
    return image
