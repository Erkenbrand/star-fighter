import pygame

from pyogi.color import normalize


def up(surface, color, height):
    size = surface.get_size()
    dark_color = normalize((color[0] - 50, color[1] - 50, color[2] - 50))
    heel_color = normalize((color[0] + 50, color[1] + 50, color[2] + 50))
    for h in range(height):
        pygame.draw.line(surface, heel_color, (h, h), (size[0] - 1 - h, h))
        pygame.draw.line(surface, dark_color, (size[0] - 1 - h, h),
                         (size[0] - 1 - h, size[1] - 1 - h))
        pygame.draw.line(surface, dark_color,
                         (size[0] - 1 - h, size[1] - 1 - h),
                         (h, size[1] - 1 - h))
        pygame.draw.line(surface, heel_color, (h, size[1]- 1 - h), (h, h))

def down(surface, color, height):
    size = surface.get_size()
    dark_color = normalize((color[0] - 50, color[1] - 50, color[2] - 50))
    heel_color = normalize((color[0] + 50, color[1] + 50, color[2] + 50))
    for h in range(height):
        pygame.draw.line(surface, dark_color, (h, h), (size[0] - 1 - h, h))
        pygame.draw.line(surface, heel_color, (size[0] - 1 - h, h),
                         (size[0] - 1 - h, size[1] - 1 - h))
        pygame.draw.line(surface, heel_color,
                         (size[0] - 1 - h, size[1] - 1 - h),
                         (h, size[1] - 1 - h))
        pygame.draw.line(surface, dark_color, (h, size[1]- 1 - h), (h, h))