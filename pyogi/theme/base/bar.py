import pygame

from pyogi.color import normalize

def down(image, color, height):
    size = image.get_size()
    new_rect = pygame.Rect((0, 0), size)
    dark_color = color
    try:
        color[3]
        for i in range(height):
            pygame.draw.rect(image, dark_color, new_rect, 1)
            dark_color = normalize((color[0] - 20 * (i + 1),
                                         color[1] - 20 * (i + 1),
                                         color[2] - 20 * (i + 1))) + (color[3],)
            new_rect.inflate_ip(-2, -2)
    except IndexError:
        for i in range(height):
            pygame.draw.rect(image, dark_color, new_rect, 1)
            dark_color = normalize((color[0] - 20 * (i + 1),
                                         color[1] - 20 * (i + 1),
                                         color[2] - 20 * (i + 1)))
            new_rect.inflate_ip(-2, -2)
    pygame.draw.rect(image, dark_color, new_rect)

def up(image, color, height):
    size = image.get_size()
    new_rect = pygame.Rect((0, 0), size)
    try:
        color[3]
        for i in range(height):
            hel_color = normalize((color[0] - 30 * (height - i),
                          color[1] - 30 * (height - i),
                          color[2] - 30 * (height - i))) + (color[3],)
            pygame.draw.rect(image, hel_color, new_rect, 1)
            new_rect.inflate_ip(-2, -2)
    except IndexError:
        for i in range(height):
            hel_color = normalize((color[0] - 30 * (height - i),
                          color[1] - 30 * (height - i),
                          color[2] - 30 * (height - i)))
            pygame.draw.rect(image, hel_color, new_rect, 1)
            new_rect.inflate_ip(-2, -2)
    pygame.draw.rect(image, color, new_rect)
