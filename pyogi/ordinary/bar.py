import pygame
from pygame.locals import *

from pyogi.basics.widget import Widget
from pyogi import Text


class Controller(Widget):

    def __init__(self, size, height=2, **config):
        Widget.__init__(self, **config)

        self._height = height
        self.geometry = pygame.Rect((0, 0), (0, 0))
        self.size = size

    def create(self):
        Widget.create(self)
        self._color = self._master._config.get("widget_fg",
                                               self._theme.colors.widget_fg)
        self.look = self._theme.bar.up

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        self._height = value
        self.change()

    def update(self):
        try:
            self._color[3]
            self._image = pygame.Surface(self.size).convert_alpha()
        except IndexError:
            self._image = pygame.Surface(self.size)
        self._image.fill(self._color)
        self.look(self._image, self._color, self._height)


class BaseBare(Widget):

    def __init__(self, maximal, minimal, step, size, height, font, event,
                 **config):
        """Enthaelt Code die alle Balken gemeinsam haben."""
        Widget.__init__(self, **config)

        self._color = None
        self._maximal = maximal - minimal
        self._minimal = minimal
        self._value = minimal
        self._step = step if step <= self._maximal else self._maximal
        self._height = height
        self.geometry = pygame.Rect((0, 0), (0, 0))
        self.size = size
        self._image = None
        self.look = None
        self.event = event
        self.bind = None
        if font:
            self.label = Text(str(minimal), font, **config)
            self.add(self.label)
        else:
            self.label = None

    def create(self):
        Widget.create(self)

        self._color = self._config.get("widget_bg",
                                       self._theme.colors.widget_bg)
        self.look = self._theme.bar.down
        if self.label:
            self.label.create()

    def update(self):
        try:
            self._color[3]
            self._image = pygame.Surface(self.size).convert_alpha()
        except IndexError:
            self._image = pygame.Surface(self.size)
        self.look(self._image, self._color, self._height)
        if self._active:
            self.controller.draw(self._image)
        if self.label:
            self.label.text = str(self._value)
            self.label.set_side(self.local_side("center"), "center")
            self.label.draw(self._image)

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        self._height = value
        self.controller.height = value
        self.change()

    @property
    def maximal(self):
        return self._maximal + self._minimal

    @maximal.setter
    def maximal(self, value):
        self._maximal = value - self._minimal
        old_vaul = self._value
        self._value = 0
        self.set(old_vaul)
        self.change()

    @property
    def minimal(self):
        return self._minimal

    @minimal.setter
    def minimal(self, value):
        maximal = self.maximal
        self._minimal = value
        self.maximal = maximal

    @property
    def step(self):
        return self._step

    @step.setter
    def step(self, value):
        self._step = value if value <= self._maximal else self._maximal
        self.change()

    def back(self):
        """Bewegt den Balken einen schritt zurueck."""

        self.set(self._value - self._step)

    def forward(self):
        """Bewegt den Balken einen schritt vor."""

        self.set(self._value + self._step)

    def get(self):
        return self._value

    def set(self, value):
        """Setzt den Balken auf den uebergebenen wert,
        ist der wert groeser als maximal wird er auf maximal gesetzt,
        ist er kleiner als minimal auf minimal.
        """

        if value == self._value:
            return
        elif value > self.maximal:
            self._value = self.maximal
        elif value < self._minimal:
            self._value = self._minimal
        else:
            self._value = value
        if self.event:
            self.event(-self.get())
        self.change()


class XBar(BaseBare):

    def __init__(self, maximal=10, minimal=0, step=1, size=(100, 16), height=3,
                 font=None, event=None, **config):
        """Ein an der x Achse ausgerichteter Balken mit Schieberegler."""

        BaseBare.__init__(self, maximal, minimal, step, size, height, font, event,
                          **config)

        self.controller = Controller((0, 0), self._height,
                                     **config)
        self.add(self.controller)
        self.bind_event((1, 0, 0), self.set_mouse)
        self.bind_event_mode(MOUSEBUTTONDOWN, 4, self.back)
        self.bind_event_mode(MOUSEBUTTONDOWN, 5, self.forward)

    def bind_widget(self, widget):
        self.bind = widget
        self.bind.links.add(self)
        #self.event = widget.set_xscroll########################
        self.change()

    def create(self):
        BaseBare.create(self)
        self.controller.create()

    def update(self):
        if self._maximal > 0 < self.step:
            self._active = True
            controller_size = (self.w / ((self._maximal + 1) / self._step), self.h)
            if controller_size[0] < 1:
                controller_size = (1, self.h)
            self.controller.size = controller_size
            self.distance = (self.w - self.controller.w) / self._maximal
            self.controller.set_side(int((
                self._value - self._minimal) * self.distance + 0.5), "x")
        else:
            self._active = False
        BaseBare.update(self)

    def set_mouse(self):
        """Setzt den Balken auf die Mausposition."""

        if self._maximal > 0 < self.distance:
            self.set(int((int(self.mouse_pos[0] - self.controller.w / 2 + 0.5)
                          ) / self.distance + 0.5 + self._minimal))


class XStatusBar(BaseBare):

    def __init__(self, maximal=10, minimal=0, step=1, size=(100, 16), height=3,
                 font=None, event=None, **config):
        """Ein an der x Achse ausgerichteter Balken"""

        BaseBare.__init__(self, maximal, minimal, step, size, height, font, event,
                          **config)

        self.controller = Controller((0, size[1]), height, **config)
        self.add(self.controller)
        self.distance = (self.geometry.w - self.controller.w) / self._maximal

    def create(self):
        BaseBare.create(self)

        self.controller.create()

    def update(self):

        self.controller.w = int((self._value - self._minimal
                                 ) * self.distance + 0.5)
        BaseBare.update(self)

    def set_mouse(self):
        """Setzt den Balken auf die Mausposition"""

        self.set(int((int(self.mouse_pos[0] - self.controller.w / 2 + 0.5)
                      ) / self.distance + 0.5 + self._minimal))

    def get(self):
        """Gibt den wert auf dem der Balken steht zurueck."""

        return (self._value, 0)


class YBar(BaseBare):

    def __init__(self, maximal=10, minimal=0, step=1, size=(16, 100), height=3,
                 font=None, event=None, **config):
        """Ein an der y Achse ausgerichteter Balken mit Schieberegler"""

        BaseBare.__init__(self, maximal, minimal, step, size, height, font,
                          event, **config)

        self.controller = Controller((0, 0), self._height,
                                     **config)
        self.add(self.controller)
        self.bind_event((1, 0, 0), self.set_mouse)
        self.bind_event_mode(MOUSEBUTTONDOWN, 4, self.back)
        self.bind_event_mode(MOUSEBUTTONDOWN, 5, self.forward)

    def bind_widget(self, widget):
        self.bind = widget
        #self.event = widget.set_yscroll
        self.change()

    def create(self):
        BaseBare.create(self)
        self.controller.create()

    def update(self):

        if self._maximal > 0 < self.step:
            self._active = True
            controller_size = (self.w, self.h / ((self._maximal + 1) / self._step))
            if controller_size[1] < 1:
                controller_size = (self.w, 1)
            self.controller.size = controller_size
            self.distance = (self.h - self.controller.h) / self._maximal
            self.controller.set_side(int((
                self._value - self._minimal) * self.distance + 0.5), "y")
        else:
            self._active = False
        BaseBare.update(self)

    def set_mouse(self):
        """Setzt den Balken auf die Mausposition,
        die als letztes im Widget gespeichert wurde.
        """

        if self._maximal > 0 < self.distance:
            self.set(int((int(self.mouse_pos[1] - self.controller.h / 2 + 0.5)
                          ) / self.distance + 0.5 + self._minimal))


class YStatusBar(BaseBare):

    def __init__(self, maximal=10, minimal=0, step=1, size=(16, 100), height=3,
                 font=None, event=None, **config):
        """Ein an der y Achse ausgerichteter Balken"""

        BaseBare.__init__(self, maximal, minimal, step, size, height, font,
                          event, **config)

        self.controller = Controller((size[0], 0), height, **config)
        self.add(self.controller)
        self.distance = (self.geometry.h - self.controller.h) / self._maximal

    def create(self):
        BaseBare.create(self)

        self.controller.create()

    def update(self):

        self.controller.h = int((self._value - self._minimal
                                 ) * self.distance + 0.5)
        BaseBare.update(self)

    def set_mouse(self):
        """Setzt den Balken auf die Mausposition"""

        self.set(int((int(self.mouse_pos[1] - self.controller.h / 2 + 0.5)
                      ) / self.distance + 0.5 + self._minimal))
