import pygame

import pyogi
from pyogi.locals import *
from pyogi.basics.event import MouseIn
from pyogi import Container


class Listbox(Container):

    def __init__(self, font, size=(64, 128), event=None, **config):
        Container.__init__(self, size,
                           sorter=pyogi.sort.Row(from_side="midtop",
                                                 to_side="midbottom"),
                           **config)
        self.font = font
        self.event = event
        self.text_bg = None
        self.focus = None
        self.select_color = None
        self.bind_event_mode(MOUSEBUTTONUP, 4, self.up)
        self.bind_event_mode(MOUSEBUTTONUP, 5, self.down)
        self.bind_event(MOUSEBUTTONUP, self.focussing)
        self.write_time = pyogi.event.InputDelayChek(600)
        self.bind_event_mode(KEYDOWN, K_UP,
                             lambda event: self.write_time(self.up))
        self.bind_event_mode(KEYDOWN, K_DOWN,
                             lambda event: self.write_time(self.down))
        self.bind_event(KEYUP, self.write_time.reset)

    def create(self):
        Container.create(self)
        self.select_color = self._config.get("select_color",
                                             self._theme.colors.select_color)
        self.in_color = self._config.get("in_color",
                                         self._theme.colors.in_color)
        self.text_bg = self._config.get("text_bg",
                                        self._theme.colors.text_bg)
        if self.focus:
            self.focus.config(text_bg=self.select_color)
            if self.event:
                self.event(self.focus.text)

    @property
    def text(self):
        if self.focus:
            return self.focus.text
        else:
            return ""

    def up(self, event=None):
        if self.focus:
            self._slaves[self._slaves.index(self.focus) - 1].select()
            if self.focus.geometry.top < 0:
                self.yscroll(self.focus.geometry.top)
            elif self.focus.geometry.bottom > self.h:
                self.yscroll(self.focus.geometry.bottom - self.h)

    def down(self, event=None):
        if self.focus:
            try:
                self._slaves[self._slaves.index(self.focus) + 1].select()
                if self.focus.geometry.bottom > self.h:
                    self.yscroll(self.focus.geometry.bottom - self.h)
            except IndexError:
                self._slaves[0].select()
                if self.focus.geometry.top < 0:
                    self.yscroll(self.focus.geometry.top)

    def add_rows(self, *text_list):
        for text in text_list:
            self.add_row(text)
        self.update_link()

    def update(self):
        self._image = pygame.Surface(self.size)
        self._image.fill(self.container_bg)
        view_test = pygame.Rect((0, 0), self.size).colliderect
        for slave in self._slaves:
            if view_test(slave.geometry):  # TODO: speichern: view_test
                slave.draw(self._image)

    def add_row(self, text):
        row = pyogi.Text(text, self.font)

        def select(event=None):
            if self.focus != row:
                if self.focus:
                    self.focus.config(text_bg=self.text_bg)
                if self.event:
                    self.event(row.text)
                self.focus = row
                row.config(text_bg=self.select_color)
            elif self.event:
                self.event(row.text)

        row.select = select

        def mose_in(event):
            if row != self.focus:
                row.config(text_bg=self.in_color)

        def mose_out(event):
            if row != self.focus:
                row.config(text_bg=self.text_bg)

        row.bind_event_mode(MOUSEBUTTONUP, 1, select)
        row.bind_event(MOUSE_GO_IN, mose_in)
        row.bind_event(MOUSE_GO_OUT, mose_out)
        self.add(row)
        if not self.focus:
            self.focus = row
            self.focus.config(text_bg=self.select_color)

    def clean(self):
        """Loescht alle Widget."""

        for widget in self._slaves[:]:
            widget.kill()
        self.focus = None
        self.update_link()
