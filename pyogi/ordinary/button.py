from pyogi.basics.button import BaseButton
from pyogi.locals import *


class Button(BaseButton):

    def __init__(self, label=None, size="auto", orientation="center",
                 event=None, state_event=None, height=3, **config):
        BaseButton.__init__(self, label, size, orientation, event, state_event,
                            height, **config)

        self.bind_event_mode(MOUSEBUTTONUP, 1, self.release)
        self.bind_event_mode(MOUSEBUTTONDOWN, 1, self.down)
        self.bind_event_mode(MOUSE_GO_IN, (1, 0, 0), self.down)
        self.bind_event(MOUSE_GO_OUT, self.up)
