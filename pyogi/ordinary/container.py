import pygame

from pyogi.locals import *
from pyogi.basics import BaseContainer, Widget


class Container(BaseContainer):

    def __init__(self, size="fill", sorter=None, **config):
        """
        Ein Container fuer Widget, er zeichnet die Widget an dem Ort,
        an dem sie mit der Methode place platziert werden
        und in der reihen folge in der sie hinzugefuegt wurden
        """

        BaseContainer.__init__(self, sorter, **config)

        self.geometry = pygame.Rect((0, 0), (0, 0))
        self.size = size
        self._color = None
        self._image = None
        self.changed = True

    def update_geometry(self):
        if self._master:
            if self.sorter:
                self.sorter(self._slaves, self)
                if self._size == "auto":
                    self.autosize()
                    self.sorter(self._slaves, self)
                elif self._size == "fill" and self.alive:
                    self.fill()
                    self.sorter(self._slaves, self)
            else:
                if self._size == "auto":
                    self.autosize()
                elif self._size == "fill" and self.alive:
                    self.fill()
            self.update_link()
            if self._master._size == "auto":
                self._master.update_geometry()
            self.change()

    def create(self):
        BaseContainer.create(self)
        self.container_bg = self._config.get("container_bg",
                                             self._theme.colors.container_bg)

    def update(self):
        self._image = pygame.Surface(self.size)
        self._image.fill(self.container_bg)
        BaseContainer.draw(self, self._image)

    draw = Widget.draw
