import pygame

from pyogi import Container
from pyogi import Text
from pyogi.basics.widget import Widget
from pyogi.locals import *


class  Knot(Text):

    def __init__(self, name, font, event=None, **config):
        Text.__init__(self, name, font, **config)

        self.entrys = []
        self.root = None
        self.event = event
        self._open = False
        self.bind_event_mode(MOUSEBUTTONUP, 1, self.open)

    def create(self):
        Text.create(self)

        self.in_color = self._config.get("in_color",
                                         self._theme.colors.in_color)
        self.select_color = self._config.get("select_colo",
                                         self._theme.colors.select_color)

    def open(self):
        if self.event:
            self.event()
        self.bind_event_mode(MOUSEBUTTONUP, 1, self.close)
        if self.entrys:
            self._open = True
            self.text = "-" + self.text[1:]
        else:
            self.config(text_bg=self.select_color)

    def close(self):
        self.bind_event_mode(MOUSEBUTTONUP, 1, self.open)
        if self._open:
            self._open = False
            self.text = "+" + self.text[1:]
        else:
            self.config(text_bg=None)

    def add_knot(self, name="", event=None):
        knot = Knot(name, self.font, event)
        if not self.entrys:
            self.text = "+" + self.text
        self.add(knot)
        knot.link_side("topleft", "bottomleft", (16, 0))
        self.entrys.append(knot)
        self.change()
        return knot

    # def draw(self, buffer):
    # 
    #     if self.changed:
    #         self.update()
    #         self.changed = False
    #     if self.visibility:
    #         Text.draw(self, buffer)
            # if self._open:
            #     for knot in self.entrys:
            #         knot.pos = self.pos
            #         knot.draw(buffer)


class Tree(Container):

    def __init__(self, font, size=(128, 128), **config):
        Container.__init__(self, size, **config)
        self.font = font

    def add_knot(self, name="", event=None):
        knot = Knot(name, self.font, event)
        if not self._slaves:
            print("11111")
            knot.link_side(self, "topleft", (20, 200))
        else:
            knot.link_side(self._slaves[-1], "topleft", "bottomleft", (0, 0))
        self.add(knot)
        self.change()
        return knot
