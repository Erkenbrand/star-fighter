
from .share import Fileshare


class Open(Fileshare):

    def __init__(self, font, size=(300, 200), path=".", file_filter={"all"},
                 hidden=True, event=None, killevent=None, serch="", **config):
        """-Ein Dialog zum oeffnen von Daten.
        -Ruft das uebergebene event,
        mit dem aktuellem Pfad und der ausgewaehlten Datei auf.
        -Mit killevent muss der Dialog beendet werden.
        -Ueber filters, muessen die gewuenschten Datenendungen, in einer liste,
        tuple oder set uebergeben werden.
        Wird der Eintrag "all" hinzugefuegt, werden alle Daten gezeigt.
        """
        Fileshare.__init__(self, "open", font, size, path, file_filter, hidden,
                           event, killevent, serch, config)
