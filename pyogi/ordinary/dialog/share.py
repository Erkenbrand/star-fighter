
#from pygame.locals import *

import pyogi
from .filemanager import FileManager


class Fileshare(pyogi.Frame):

    def __init__(self, text, font, size, path, file_filter, hidden, event,
                 killevent, serch, config):
        """"""

        container = pyogi.Container("fill")
        pyogi.Frame.__init__(self, container, size, 3, **config)

        filemanager = FileManager(font, "fill", path, event, killevent)
        open_button = pyogi.Button(pyogi.Text("open", font), "auto",
                                   event=filemanager.open)
        close_button = pyogi.Button(pyogi.Text("close", font), "auto",
                                    event=killevent)

        back = pyogi.Button(pyogi.Text("< - -", font), "auto",
                            event=filemanager.backward)
        filtering = pyogi.Dropdownlist(font, event=filemanager.set_filter)
        filtering.add_rows(*file_filter)

        bar = pyogi.Container("auto")

        path_label = filemanager.path_label
        #path_label_frame = pyogi.Frame(path_label, "auto", 4, "up")
        serch = filemanager.serch
        serch_label = pyogi.Text("Serch:", font)
        serch_frame = pyogi.Frame(serch, "auto", 3)

        x_bar = pyogi.XBar()
        x_bar.bind_widget(filemanager)
        y_bar = pyogi.YBar()
        y_bar.bind_widget(filemanager)
        up = pyogi.Button(pyogi.Text("^", font), (16, 16), event=None)
        down = pyogi.Button(pyogi.Text("v", font), (16, 16), event=None)
        left = pyogi.Button(pyogi.Text("<", font), (16, 16), event=None)
        right = pyogi.Button(pyogi.Text(">", font), (16, 16), event=None)

        up.bind_event((1, 0, 0), pyogi.event.Delay(y_bar.back, 100))
        down.bind_event((1, 0, 0), pyogi.event.Delay(y_bar.forward, 100))
        left.bind_event((1, 0, 0), pyogi.event.Delay(x_bar.back, 100))
        right.bind_event((1, 0, 0), pyogi.event.Delay(x_bar.forward, 100))########

        container.add(bar)

        bar.link_size(container, container, "topleft", "topright", mode="w")
        bar.place(filtering, (0, 0), "midright", "midright", link=True)
        pyogi.place.to(filtering, serch_frame, "midright", "midleft", link=True)
        pyogi.place.to(serch_frame, serch_label, "midright", "midleft",
                       link=True)
        pyogi.place.to(serch_label, back, "midright", "midleft",
                        link=True)

        bar.add(path_label)
        path_label.link_size(back, bar, "topleft", "midleft", mode="w")

        container.add(open_button)
        container.add(close_button)

        open_button.link_size(container, container, "midbottom", "bottomright",
                              offset=(0, -16), mode="w")
        close_button.link_size(container, container, "midbottom", "bottomleft",
                               offset=(0, -16), mode="w")

        pyogi.place.to(bar, up, "topright", "bottomright", link=True)
        pyogi.place.to(open_button, down, "bottomright", "topright", link=True)
        pyogi.place.to(down, right, "bottomright", "bottomleft", link=True)
        pyogi.place.to(close_button, left, "bottomleft", "topleft", link=True)

        container.add(filemanager)
        container.add(x_bar)
        container.add(y_bar)

        filemanager.link_size(bar, down, "bottomleft", "topleft")
        y_bar.link_size(down, up, "topright", "bottomleft")
        x_bar.link_size(left, right, "topright", "bottomleft")

    def set_bars(self):
        """Aktualisiert die Balken."""

        x, y = self.list.get_scroll()
        xway = x[1] - x[0]
        yway = y[1] - y[0]
        self.x_bar.limit = xway / 2
        self.x_bar.minimal = -(xway / 2)
        if xway > 0:
            step = 25
            self.x_bar.step = step
        else:
            self.x_bar.step = 1
        self.y_bar.limit = yway
        if yway > 0:
            step = 50
            self.y_bar.step = step
        else:
            self.y_bar.step = 1
