from .open import Open
from .save import Save
from pyogi import Gui


def set(widget, pos, side="topleft"):
    Gui.dialog = widget
    Gui.dialog_side = side
    widget._master = Gui.instance
    widget.create()
    widget.set_side(pos, side)

def move(pos):
    Gui.dialog.set_side(pos, Gui.dialog_side)

def clean():
    if Gui.dialog:
        Gui.dialog._master = None
        Gui.dialog.alive = False
        Gui.dialog.changed = False
        Gui.dialog = None
        Gui.instance.change()
