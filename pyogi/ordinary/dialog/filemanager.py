# TODO: nutze das neue pathlib
import os

from pygame.locals import *

import pyogi


class FileManager(pyogi.Listbox):

    def __init__(self, font, size=(100, 100), path=".", event=None,
                 killevent=None, hidden=True, filter_txt="all", **config):

        self._hidden = hidden
        self._filter = filter_txt
        self._event = event
        self.killevent = killevent
        self.last_select = ""
        self.open_event = pyogi.event.Doubleclick(self.open, timer=200)
        pyogi.Listbox.__init__(self, font, size, event=self.set, **config)
        self.path_label = pyogi.Label("test path", font)
        self.serch = pyogi.Entry(font, event=self.update_list, scrolable=True)
        self.open_dir(path)
        self.bind_event_mode(KEYUP, K_RETURN, self.open)
        self.bind_event_mode(KEYUP, K_BACKSPACE, self.backward)
        self.bind_event_mode(KEYUP, (K_h, KMOD_LCTRL), self.switch_hide)

    def set(self, text):

        if text == self.last_select:
            self.open_event()
        else:
            self.open_event.reset()
            self.open_event()
        self.last_select = text

    def update_list(self, event=None):
        self.clean()
        self.add_rows(*self.filter_files())

    @property
    def file_path(self):
        """Gibt den ausgewaehlten Pfad zurueck."""

        return os.path.join(self._path, self.text)

    def open(self):
        ###"""Oeffnet bei doppelklick, einen Ordner, oder fuehrt das uebergebene
        #event aus event(ausgewaehlter Pfad) und Schliesst den Dialog per killevent.
        #"""
        if not self.text:
            return

        file_path = self.file_path
        if os.path.isdir(file_path):
            try:
                self.open_dir(file_path)
            except PermissionError:
                print("keine berechtigung")
        else:
            try:
                self._event(file_path)
                if self.killevent:
                    self.killevent()
            except TypeError as err:
                print(
                    "keine verwendung fuer diese datei : " + self.text
                    )

    def open_dir(self, path):
        """Oeffnet einen Pfad und zeigt dessen Inhalt an.
        Der uebergebene Pfad, muss ein Ordner sein.
        """

        abs_path = os.path.abspath(path)
        self.files = os.listdir(abs_path)
        self.path = abs_path
        self.serch.clear()
        self.update_list()

    def backward(self):
        """Geht ein Verzeichnis tiefer."""

        self.open_dir(os.path.dirname(self._path))

    def filter_files(self):
        """Filtert die angezeigten Dateien,
        nach den ausgewaehlten und eingegebenen Einstellungen.
        """

        files = self.files

        if self._hidden:
            files = [file for file in files if not file[0] == "."]
        if self._filter != "all":
            files = [file for file in files
                     if os.path.isdir(os.path.join(self._path, file))
                     or os.path.splitext(file)[1] in self._filter]
        if self.serch.text:
            if self.serch.text[0] == "*":
                serch_pure = self.serch.text[1:]
                files = [file for file in files if serch_pure in file]
            else:
                files = [file for file in files
                         if file.startswith(self.serch.text)]
        files.sort()
        return files

    def switch_hide(self, value=None):
        """Zeigt (True) oder versteckt (False) die versteckten Dateien.
        Wird None uebergeben, wird die aktuelle Auswahl umgekehrt.
        """

        if value is None:
            self.hidden = not self._hidden
        else:
            self.hidden = value

    def set_filter(self, filter_txt={"all"}):
        self.filter = filter_txt

    @property
    def hidden(self):
        return self._hidden

    @hidden.setter
    def hidden(self, value):
        self._hidden = value
        self.update_list()

    @property
    def filter(self):
        return self._filter

    @filter.setter
    def filter(self, value={"all"}):
        self._filter = value
        self.update_list()

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value
        self.path_label.text = value
