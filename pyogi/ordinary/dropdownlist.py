import pygame

from pyogi import Label, Listbox
import pyogi
from pyogi.locals import *


class Dropdownlist(Label):

    def __init__(self, font, width=128, box_height=128, event=None):
        """Eine aufklappbare Listbox die im dialog angezeigt wird."""

        Label.__init__(self, "", font, (width, font.get_sized_height()))

        self.event = event
        self.end_event = pyogi.event.Doubleclick(pyogi.dialog.clean, None, 200)

        self.list = Listbox(font, size="fill", event=self.set_chose)
        self.frame = pyogi.Frame(self.list, (width, box_height), 3)
        self.frame.bind_event(MOUSE_GO_OUT, pyogi.dialog.clean)

        self.bind_event_mode(MOUSEBUTTONUP, 1, self.show_box)
        self.bind_event_mode(MOUSEBUTTONDOWN, 5, self.list.down)
        self.bind_event_mode(MOUSEBUTTONDOWN, 4, self.list.up)

    def add_row(self, text):
        """Fuegt einen eintrag hinzu"""

        self.list.add_row(text)
        if not self.text:
            self.text = text
            if self.event:
                self.event(text)

    def add_rows(self, *rows):
        """Fuegt mehrere eintraege hinzu"""

        self.list.add_rows(*rows)
        if not self.text:
            self.text = self.list.text
            if self.event:
                self.event(self.text)

    def get_chose(self):
        """Gibt den ausgewaehlten Text zurueck. """

        return self.text

    def set_chose(self, text):

        if self.text == text:
            self.end_event()
        else:
            self.end_event.reset()
            self.end_event()
        self.text = text
        if self.event:
            self.event(text)

    def show_box(self, event):
        if pyogi.Gui.dialog != self.list:
            pyogi.dialog.set(self.frame, self.global_side("bottomleft"))
        else:
            pyogi.dialog.clean()
