import pygame

from pyogi.locals import DEFAULT_THEME
from pyogi.basics.widget import Widget
from pyogi.basics.event import MouseIn
from pyogi.basics import SingleContainer


class Frame(SingleContainer):

    def __init__(self, widget, size="auto", width=5, look="down", **config):
        """Container fuer ein einzelnes Widget das einen Rahmen zeichnet."""
        SingleContainer.__init__(self, size=size, orientation="center",
                                 **config)

        self.sub_geometry = pygame.Rect((0, 0, self.w - width * 2,
                                         self.h - width * 2))
        self._frame = width
        self.look = look
        self.add(widget)

    def create(self):
        SingleContainer.create(self)
        self.container_bg = self._config.get("container_bg",
                                             self._theme.colors.container_bg)

    @property
    def fill_size(self):
        """Gibt die Groesse, die von einem widget befuellt werden kann zurueck.
        """
        if self._frame:
            return (self.w - self._frame * 2, self.h - self._frame * 2)
        return self.geometry.size

    def autosize(self):
        if self._slave:
            self.geometry.size = (self._slave.w + self._frame * 2,
                                  self._slave.h + self._frame * 2)

    def update(self):
        self._image = pygame.Surface(self.geometry.size)
        self._image.fill(self.container_bg)
        getattr(self._theme.frame, self.look)(self._image, self.container_bg,
                                              self._frame)

        if self._slave and self.w > self._frame * 2 < self.h:
            subsurface = self._image.subsurface(self._frame, self._frame,
                                                self.w - self._frame * 2,
                                                self.h - self._frame * 2)
            if self.orientation:
                pos = getattr(subsurface.get_rect(), self.orientation)
                setattr(self._slave.geometry, self.orientation, pos)
            SingleContainer.draw(self, subsurface)

    mouse_test = SingleContainer.frame_mouse_test

    def global_master_pos(self, pos):
        """Gibt die Position, im widget, auf master Ebene zurueck."""
        return (pos[0] + self.x + self._frame, pos[1] + self.y + self._frame)

    draw = Widget.draw
