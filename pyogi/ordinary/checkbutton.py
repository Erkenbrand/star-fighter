import pygame

from pyogi.locals import *
from pyogi.basics.button import BaseButton


class Connecter:

    def __init__(self, widget):
        self.active = widget
        widget.switch()
        self.data = set()
        self.data.add(widget)

    def set(self, widget):
        if self.active != widget:
            self.active.up()
            self.active = widget

    def merge(self, value):
        self.data |= value

    def add(self, value):
        self.data.add(value)


class Checkbutton(BaseButton):

    def __init__(self, label=None, size="auto", orientation="center",
                 event=None, release=None, height=3, **config):
        """
        height : gibt die Hoehe des Buttons an (Rahmen dicke)
        size : wird kein wert uebergeben wird die groesse des Textes + 2 * height
        genommen
        """
        BaseButton.__init__(self, label, size, orientation, None, event, height,
                            **config)
        self.connection = None
        self.bind_event_mode(MOUSEBUTTONUP, 1, self.switch)

    def switch(self, event=None):
        if not self.state:
            if self.connection:
                if self.connection.active != self:
                    self.connection.set(self)
                    self.down()
                return
            self.down()
        elif self.connection:
            return
        else:
            self.up()

    def connect(self, other):
        """
        bindet 2 Checkbuttons und ihre bestehenden Verbindungen aneinander.
        Es kann immer nur ein Button einer Verbindungen gedrueckt sein,
        alle anderen werden zurueck gesetzt
        """

        if self.connection:
            if other.connection:
                self.connection.merge(other.connection)
            else:
                other.connection = self.connection
                self.connection.add(other)
        else:
            if other.connection:
                other.connection.add(self)
                self.connection = other.connection
            else:
                self.connection = Connecter(self)
                self.connection.add(other)
                other.connection = self.connection
