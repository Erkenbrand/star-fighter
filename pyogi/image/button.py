import os

import pygame

from pyogi.image import Imagestore
from pyogi.basics.widget import Widget
from pyogi.locals import *


class SurfaceButton(Widget):

    def __init__(self, images, event=None, mouse_in=False):
        """
        ein Button bestehend aus 2 oder 3 Images
        image1 = ungeduekter Button
        image2 = gedruekter Button
        image3 = ist optonal und representiert den button wen die Maus drinnen ist
        event = das ereignis das beim loslassen des linken mouse buttons aufgerufen wird
        mouse_in = ist mouse_in auf False wird image 3 nicht geladen
        """
        Widget.__init__(self)

        self.geometry = pygame.Rect((0, 0), (0, 0))
        self.event = event
        self._state = 0
        self.bind_event(MOUSE_GO_OUT, lambda: setattr(self, "state", 0))
        self.bind_event_mode(MOUSE_GO_IN, (1, 0, 0),
                              lambda: setattr(self, "state", 1))
        self.bind_event_mode(MOUSEBUTTONDOWN, 1,
                              lambda: setattr(self, "state", 1))
        self.bind_event_mode(MOUSEBUTTONUP, 1, self.up)
        self.set_images(images, mouse_in)

    def up(self):
        """setzt den Status auf mouse_in (== 2)"""

        self.state = 2
        if self.event:
            self.event()

    def set_images(self, images, go_in=False):
        """setzt neue images"""

        self.images = images
        self.size = images[0].get_size()
        if go_in:
            try:
                images[2]
                self.bind_event_mode(MOUSE_GO_IN, (0, 0, 0),
                                  lambda: setattr(self, "state", 2))
            except IndexError:
                self.images.append(images[0])
        else:
            self.images.append(images[0])

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value
        self.change()

    def draw(self, buffer):
        if self.visibility:if self.changed:
            self.update()
            self.changed = False
        if self.visibility:
            buffer.blit(self.images[self._state], self.geometry)


class Image_button(SurfaceButton):

    def __init__(self, path, event=None, mouse_in=False):
        """
        ein Button bestehend aus 2 oder 3 Images
        image1 = die angegebene datei als ungeduekter Button
        image2 = die angegebene datei + "DOWN" als gedruekter Button
        image3 = ist optonal und representiert den button wen die Maus drinnen ist
        event = das ereignis das beim loslassen des linken mouse buttons aufgerufen wird
        mouse_in = ist mouse_in auf False wird image 3 nicht geladen
        """
        SurfaceButton.__init__(self, path, event, mouse_in)

        self._path = path
        self._state = 0
        self.event = event
        self.bind_event(MOUSE_GO_OUT, lambda: setattr(self, "state", 0))
        self.bind_event_mode(MOUSE_GO_IN, (1, 0, 0),
                              lambda: setattr(self, "state", 1))
        self.bind_event_mode(MOUSEBUTTONDOWN, 1,
                              lambda: setattr(self, "state", 1))
        self.bind_event_mode(MOUSEBUTTONUP, 1, self.up)

    def set_images(self, path, go_in=False):
        """setzt neue images"""

        SurfaceButton.set_images(self, self.load_images(path), go_in)

    def load_images(self, path, mouse_in=False):
        """
        ladet einen Neuen Image Satz, ist image3 nicht vorhanden,
        wierd eine ausnahme ausgeloest
        """

        splitet = os.path.splitext(path)
        pressed = "%s%s%s" % (splitet[0], "DOWN", splitet[1])
        mouse_test = "%s%s%s" % (splitet[0], "IN", splitet[1])
        images = [Imagestore.get_image(path),
                       Imagestore.get_image(pressed)]
        try:
            images.append(Imagestore.get_image(mouse_test))
            return images
        except pygame.error:
            return images
