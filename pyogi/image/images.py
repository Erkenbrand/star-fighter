import pygame

from pyogi.basics.widget import Widget
from pyogi.image import Imagestore
from pyogi.locals import *
from pyogi.basics.event import MouseIn


class BaseImage(Widget):

    def __init__(self, image):
        """Ein Widget das ein uebergebenes Surface in der Gui representiert"""
        Widget.__init__(self)
        self._image = image
        self.geometry = image.get_rect()

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, value):
        self._image = value
        self.size = value.get_size()


class Image(BaseImage):

    def __init__(self, path):
        """
        Das Image wird im Imagestore mit dem nahmen path gespeichert
        """
        BaseImage.__init__(self, Imagestore.get_image(path))
        self._path = path

    @property
    def path(self, value):
        return self._path

    @path.setter
    def path(self, value):
        if value != self._path:
            self._path = value
            self.image = Imagestore.get_image(self._path)


class Animation(Image):

    def __init__(self, path, size, w, h, time=50):
        Image.__init__(self, path)
        if size == "auto":
            self.frame_w = self.w // w
            self.frame_h = self.h // h
            self.size = (self.frame_w, self.frame_h)
        else:
            self.size = size
            self.frame_w, self.frame_h = size
        self.shid_w = w
        self.shid_h = h
        self.view = (0, 0)
        self.cal_time = time
        self.next_call = 0

    @Image.image.setter
    def image(self, value):
        self._image = value

    def draw(self, buffer):
        if self.changed:
            self.update()
            self.changed = False
        if self.visibility:
            buffer.blit(self._image, self.geometry,
                        pygame.Rect((self.frame_w * self.view[0],
                                     self.frame_h * self.view[1]),
                                    (self.frame_w, self.frame_h)))
            tick = pygame.time.get_ticks()
            if tick >= self.next_call:
                if self.view[0] + 1 == self.shid_w:
                    if self.view[1] + 1 == self.shid_h:
                        self.view = (0, 0)
                    else:
                        self.view = (0, self.view[1] + 1)
                else:
                    self.view = (self.view[0] + 1, self.view[1])
                self.next_call = tick + self.cal_time
            self._master.bloking()
