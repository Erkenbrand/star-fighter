"""Alles zur bewaffnug."""

import os
import math

import pygame

import image
import vector
import move
import pyplay


class Geschuetz:
    """Repraesentiert einen Geschuetzturm eines Schiffes."""

    def __init__(self, dater):
        """Gibt einen Geschuetzturm fuer ein Schiff zuruek."""
        self.pos = vector.Rdot(*dater["pos"], math.radians(dater["rotation"]))
        self.waffen = []
        self.rotierbar = dater["rotierbar"]
        self.rotationmin = self.pos.radians
        rotationmax = dater.get("rotationmax", None)
        if rotationmax:
            self.rotationmax = math.radians(rotationmax)
        self.kind = dater["kind"]
        self.stufe = dater["stufe"]
        self.hitze = 0
        self.layer = 0
        try:
            for waffe in dater["gun"]:
                self.add(Waffe(waffe))
        except TypeError:
            print("Keine Waffe mounted")

    @property
    def mounted(self):
        return self.waffen[self.layer] if self.waffen else None

    @mounted.setter
    def mounted(self, value):
        self.waffen.append(value)

    def next(self):
        """Wechselt zur naechsten Waffe."""
        laenge = len(self.waffen)
        if self.layer < laenge-1:
            self.layer += 1
        else:
            self.layer = 0

    def previous(self):
        """Wechselt zur vorherigen Waffe."""
        laenge = len(self.waffen)-1
        if self.layer > 0:
            self.layer -= 1
        else:
            self.layer = laenge

    def add(self, weapon):
        """Fuegt eine Waffe zum Geschuetz hinzu.

        Montiert sie wen noch keins montiert ist.
        """
        self.waffen.append(weapon)
        if not self.mounted:
            self.mounted = weapon

    def rotate(self, radians):
        """Setzt die rotation des Geschuetzturms."""
        self.pos.radians = radians

    def feuer(self, group, ship_pos, ziel_pos, ziel, game):
        """Feuert die Montierte Waffe ab wen das Geschuetzt abgekuehlt ist."""
        if self.hitze <= 0 and self.mounted:
            waffe = self.mounted
            self.hitze = waffe.hitze
            rotated = vector.rotate(self.pos.x, self.pos.y, ship_pos.radians)
            pos = ship_pos + (rotated[0], rotated[1], self.pos.radians)
            if self.rotierbar:
                pos.radians = vector.radian(pos, ziel_pos)
                try:
                    if self.rotationmax < pos.radians:
                        pos.radians = self.rotationmax
                    elif self.rotationmin > pos.radians:
                        pos.radians = self.rotationmin
                except AttributeError:
                    pass
            Geschoss(group, pos, self.kind, ziel, waffe, game)

    def update(self, past_time):
        self.hitze -= past_time


class Waffe:
    """Waffe die auf einem Geschuetzturm montiert wird."""

    def __init__(self, dater: dict):
        """Waffe die auf einem Geschuetzturm montiert wird.

        dater: dict{image, explosion, schaden, hitze, speed, movement, stufe}.
        """
        self.image = image.get(os.path.join("daten", "Waffen", "Images",
                                            dater["image"]))
        self.explosion = image.get(os.path.join("daten", "effects", "Images",
                                                dater["explosion"]))
        self.schaden = dater["schaden"]
        self.hitze = dater["hitze"]
        self.speed = dater["speed"]
        self.movement = move.moves[dater["movement"]]
        self.stufe = dater["stufe"]
        self.dater = dater
        self.sound_fire = os.path.join("daten", "Sound",
                                       dater["sound_fire"])


class Geschoss(pygame.sprite.Sprite, move.MoveBase):
    """Einzelnes Geschoss."""

    def __init__(self, gruppen, rdot, kind, ziel, waffe, game):
        """Einzelnes Geschoss das seine Daten von einer Waffe bezieht."""
        pygame.sprite.Sprite.__init__(self, *gruppen)
        self.game = game
        self.kind = kind
        self.schaden = waffe.schaden
        self.explosion = waffe.explosion
        self.org_image = waffe.image
        self.image = pygame.transform.rotate(self.org_image, -rdot.degrees)
        self.movement = waffe.movement
        self.rect = self.image.get_rect(center=rdot.pos)
        move.MoveBase.__init__(self, rdot.pos, rdot.radians, waffe.dater)
        self.ziel = ziel
        pyplay.play(waffe.sound_fire)

    def update(self, past_time):
        if self.game.live_field.colliderect(self.rect):
            self.movement(self, past_time)
        else:
            self.kill()
