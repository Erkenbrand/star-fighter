"""Kurze Hilfsfunktionen."""

import yaml


def compare_point1(p1):
    """Funktion fuer list.sort, sortiert eine liste nach seinem zweitem element."""
    return p1[1]


def open_yaml(path=""):
    """Importiert eine yaml Datei."""
    object_file = open(path, "r")
    yaml_dater = yaml.load(object_file)
    object_file.close()
    return yaml_dater


def dump_yaml(ob, path):
    """Speichert ob in der yaml Datei path."""
    try:
        file = open(path, "w")
        yaml.dump(ob, file)
        file.close()
    except AttributeError:
        print("Error konnte file nicht schreiben")
