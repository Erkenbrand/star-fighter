
from pygame.locals import *
import pygame

#import debug_memori
#import gc


class World:

    def __init__(self, name="pygame Fenster", resolution=(600, 600),
                 flags=RESIZABLE):
        """
        Die Wurzel des kompletten Programms,
        die immer die aktuell zugewiesene Szene darstellt.
        """

        self.scene = None
        pygame.key.set_repeat(1, 1)
        pygame.mouse.set_visible(1)
        self.clock = pygame.time.Clock()
        self.resolution = resolution
        self.flags = flags
        self.image = pygame.display.set_mode(self.resolution, self.flags)
        pygame.display.set_caption(name)

    def set_scene(self, scene):
        """Hier wird eine neue darzustellende Szene uebergeben."""

        scene.master = self
        scene.screen_array = self.image.get_rect()
        if not scene.fps:
            scene.fps = self.scene.fps
        try:
            del self.scene.master
        except AttributeError:
            print("keine scene zum loeschen vorhanden.")
        self.scene = scene
        self.key_dict = {MOUSEMOTION: scene.mouse_motion,
                         MOUSEBUTTONUP:  scene.mouse_up,
                         MOUSEBUTTONDOWN: scene.mouse_down,
                         KEYDOWN: scene.key_down,
                         VIDEORESIZE: self.resize,
                         KEYUP: scene.key_up}
        #gc.collect(2)
        #print(gc.garbage)

    def input_handling(self):
        """Hier werden die Input Methoden der uebergebenen Szene aufgerufen."""

        for py_event in pygame.event.get():
            event = self.key_dict.get(py_event.type, False)
            self.scene.event(py_event)
            if event:
                event(py_event)

    def resize(self, event):
        """
        Wird beim verendern der groesse, des pygame.display aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        self.resolution = event.size
        self.image = pygame.display.set_mode(event.size, self.flags)
        self.scene.screen_array = self.image.get_rect
        self.scene.resize(event)

    def set_flags(self, flags):
        self.flags = flags
        self.image = pygame.display.set_mode(self.resolution, self.flags)

    def main(self):
        """Startet das Programm."""

        input_handling = self.input_handling
        clock = self.clock
        while self.scene:
            input_handling()
            self.scene.update(
                past_time=clock.tick(self.scene.fps))
            self.scene.draw(self.image)
            pygame.display.flip()
            pygame.display.set_caption(str(int(clock.get_fps())))
