
from pygame.locals import *


class Scene:
    """
    Die Szene die von der Welt behandelt wird,
    alle Szenen sollten von dieser klasse erben.
    """
    def __init__(self, fps=None):
        """Erstellt eine Scene die einer Welt übergeben werden muss."""
        self.screen_array = None
        self.master = None
        self.fps = fps

    def update(self, past_time):
        """
        Hier koennen Updates durchgefuehrt werden.
        Es steht der Parameter past_time zur Verfuegung.
        """
        pass

    def draw(self, image):
        """Hier muss die Szene auf self.image gezeichnet werden."""
        pass

    def event(self, event):
        """
        Wird fuer jedes event das von pygame.event.get() zurueckgegebenen wird,
        aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        pass

    def key_down(self, event):
        """
        Wird beim druecken einer Taste aufgerufen. Der Parameter event,
        kann zur Behandlung des entsprechenden pygame events benutzt werden.
        """
        pass

    def key_up(self, event):
        """
        Wird beim loslassen einer Taste aufgerufen. Der Parameter event,
        kann zur Behandlung des entsprechenden pygame events benutzt werden.
        """
        pass

    def mouse_motion(self, event):
        """
        Wird beim bewegen der Maus, ueber dem pygame Fenster aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        pass

    def mouse_up(self, event):
        """
        Wird beim loslasen eines Maus buttons aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        pass

    def mouse_down(self, event):
        """
        Wird beim druecken eines Maus buttons aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        pass

    def set_flags(self, flags):
        if self.master:
            self.master.set_flags(flags)

    def set_fps(self, fps):
        self.fps = fps

    def resize(self, event):
        """
        Wird beim verendern der groesse, des pygame.display aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        pass

    def new_scene(self, scene):
        self.master.set_scene(scene)
