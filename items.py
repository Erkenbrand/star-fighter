"""Aufsammle bare und drop bare Gegenstaende."""

import os

import pygame

import move
import image
import animator


class Item(pygame.sprite.Sprite, move.MoveBase):
    """Sammel barer Gegenstand z.b. Reparaturkit oder Geld."""

    def __init__(self, game, gruppen, dater):
        """Sammel barer Gegenstand z.b. Reparaturkit oder Geld."""
        pygame.sprite.Sprite.__init__(self, *gruppen)
        move.MoveBase.__init__(self, (0, 0), 0, dater)
        self.game = game
        self.wert = dater["wert"]
        self.movement = self.linear
        if dater.get("animation", False):
            self.image = image.get(os.path.join("daten", "items", "Sheets",
                                                dater["image"]))
            animator.linear(self, self.image, dater["animation"])
        else:
            self.image = image.get(os.path.join("daten", "items", "Images",
                                                dater["image"]))
        self.rect = self.image.get_rect()
        self.rotate = False

    def update(self, past_time):
        if self.game.live_field.contains(self.rect):
            self.movement(past_time)
        else:
            self.kill()
