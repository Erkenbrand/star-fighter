"""Inventar Verwaltung zum ausruesten und Auswahl eines Raumschiffes"""

import os

import pygame.gfxdraw

import pyogi
from pyogi.locals import *

import image
from config import DEFAULTFONT


def compare_price(dater):
    """Sortierfunktion um eine liste von Dictionary nach "price" zu sortieren"""
    return dater["price"]


class Inventory:
    """Verwaltung zum ausruesten und Auswahl eines Raumschiffes"""

    def __init__(self):
        """Verwaltung zum ausruesten und Auswahl eines Raumschiffes.

        Zur vererbung in Verwaltung."""
        self.gun_icon = image.get(os.path.join("daten", "Gui_images",
                                               "gun_icon.png"))
        self.rocket_icon = image.get(os.path.join("daten", "Gui_images",
                                                  "rocket_icon.png"))
        self.both_icon = image.get(os.path.join("daten", "Gui_images",
                                                "both_icon.png"))
        self.weapon_layer = 1
        self.weapon_next = pyogi.Button(pyogi.Text(">>", DEFAULTFONT),
                                        event=self.next_layer)
        self.weapon_layer_label = pyogi.Text("1", DEFAULTFONT)
        self.weapon_previous = pyogi.Button(pyogi.Text("<<", DEFAULTFONT),
                                            event=self.previous_layer)

    def select_ship(self, simbol, dater, buy=False):
        """Auswahl und makirung eines Raumschiffes"""
        self.weapon_layer = 1
        self.weapon_layer_label.text = str(1)
        if buy:
            event = self.buy_ship_dialog
        else:
            event = self.set_ship
        def select():
            if self.selectet:
                if self.selectet[1] == dater:
                    return
                self.selectet[0].remove_composer()
            self.admin.clean()
            pyogi.dialog.clean()
            simbol.composit(pyogi.composit.Frame(collor=(255, 0, 0)))
            self.selectet = (simbol, dater)
            self.set_holo()
            event(dater)
        return select

    def show_inventory(self, state):
        """Initialisiert das Inventar.

        state ist der zustand des Checkbuttons.
        """
        if not state:
            return
        self.clean_gui()
        simbols = []
        path = os.path.join("daten", "Schiffe")
        for ship in sorted(self.player.ships, key=compare_price):

            simbol = pyogi.images.Image(os.path.join(path, "Images",
                                                     ship["image"]))
            select = self.select_ship(simbol, ship)
            simbol.bind_event_mode(MOUSEBUTTONUP, 1, select)
            if self.player._select:
                if self.player._select == ship:
                    select()
            else:
                self.player._select = ship
                select()
            simbols.append(simbol)
        self.left_bar.add_all(simbols)
        self.gui.add_all((self.weapon_next, self.weapon_layer_label,
                          self.weapon_previous))
        self.weapon_next.link_side(self.right_bar, "topright", "topleft")
        self.weapon_layer_label.link_side(self.weapon_next, "topright",
                                          "topleft")
        self.weapon_previous.link_side(self.weapon_layer_label, "topright",
                                       "topleft")

    def montiere(self, waffe, turret, icon, simbol=None):
        """Fuegt eine Waffe zum schiffe hinzu."""
        self.demontiere(turret, icon._master, simbol)
        self.player.weapons.remove(waffe)
        simbol.image = icon.slave.image
        icon.kill()
        turret["gun"].append(waffe)

    def demontiere(self, turret, container, simbol=None):
        """Entfernt die Waffe des uebergeben Geschuetzturms."""
        if turret["gun"] and len(turret["gun"]) > self.weapon_layer - 1:
            if turret["kind"] == "e":
                simbol.image = self.gun_icon
            elif turret["kind"] == "r":
                simbol.image = self.rocket_icon
            else:
                simbol.image = self.both_icon
            old_gun = turret["gun"][self.weapon_layer - 1]
            del turret["gun"][self.weapon_layer - 1]
            self.player.weapons.append(old_gun)
            weapon_image = pyogi.images.Image(os.path.join("daten", "Waffen",
                                                           "Images",
                                                           old_gun["image"]))
            frame = pyogi.Frame(weapon_image)
            weapon_image.bind_event_mode(MOUSEBUTTONUP, 1,
                                         lambda weapon=old_gun, turret=turret,
                                                frame=frame:
                                         self.montiere(weapon, turret, frame,
                                                       simbol))
            frame.bind_event_mode(MOUSEBUTTONUP, 1,
                                  lambda weapon=old_gun, turret=turret,
                                         frame=frame:
                                  self.montiere(weapon, turret, frame, simbol))
            container.remove(self.free)
            container.add(frame)
            container.add(self.free)

    def set_ship(self, dater):
        """Setzt das Schiff das der Spieler ausgewaehlt hat."""
        self.player._select = dater
        self.set_guns(dater)

    def set_guns(self, dater):
        """Zeigt die Geschuetztuerme des vom Spieler ausgewaehltem Schiffes."""
        self.right_bar.clean()

        simbols = []
        for turret in dater["geschütze"]:
            if turret["kind"] == "e":
                simbol = pyogi.images.BaseImage(self.gun_icon)
            elif turret["kind"] == "r":
                simbol = pyogi.images.BaseImage(self.rocket_icon)
            else:
                simbol = pyogi.images.BaseImage(self.both_icon)
            frame = pyogi.Frame(simbol)
            if turret["gun"] and len(turret["gun"]) > self.weapon_layer - 1:
                simbol.image = image.get(
                    os.path.join("daten", "Waffen", "Images",
                                 turret["gun"][self.weapon_layer - 1]["image"]))
            event = lambda simbol=simbol, turret=turret: self.weapon_dialog(
                simbol, turret)
            frame.bind_event_mode(MOUSEBUTTONUP, 1, event)
            simbol.bind_event_mode(MOUSEBUTTONUP, 1, event)
            simbol.bind_event(MOUSE_GO_IN,
                              lambda pos=turret["pos"]: self.set_mark(pos))
            frame.bind_event(MOUSE_GO_IN,
                             lambda pos=turret["pos"]: self.set_mark(pos))
            frame.bind_event(MOUSE_GO_OUT, self.release_mark)
            simbols.append(frame)
        self.right_bar.add_all(simbols)

    def set_mark(self, pos):
        """Zeigt die position des ausgewaehltem Geschuetzturms am Schiff."""
        def mark(image, ship_pos):
            new_pos = (pos[0] * 2 + ship_pos[0], pos[1] * 2 + ship_pos[1])
            pygame.gfxdraw.aacircle(image, *new_pos, 8, (255, 0, 0))
        self.weapon_mark = mark

    def release_mark(self):
        """Entfernt die Markierung eines Geschuetzturms am Schiff."""
        self.weapon_mark = None

    def weapon_dialog(self, simbol, turret):
        """Zeigt die Waffen die der Spieler gekauft hat und

        die für diese Geschuetzklasse zur Auswahl stehen.
        """
        path = os.path.join("daten", "Waffen", "Images")
        waffen = set()
        for weapon in self.player.weapons:
            if weapon["kind"] in turret["kind"] and \
               weapon["stufe"] <= turret["stufe"]:
                image = pyogi.images.Image(os.path.join(path, weapon["image"]))
                frame = pyogi.Frame(image)
                image.bind_event_mode(MOUSEBUTTONUP, 1,
                                      lambda weapon=weapon, turret=turret,
                                             frame=frame:
                                      self.montiere(weapon, turret, frame,
                                                    simbol))
                frame.bind_event_mode(MOUSEBUTTONUP, 1,
                                      lambda weapon=weapon, turret=turret,
                                             frame=frame:
                                      self.montiere(weapon, turret, frame,
                                                    simbol))
                waffen.add(frame)
        container = pyogi.Container(size="auto", sorter=pyogi.sort.Row())
        label = pyogi.Label("X", DEFAULTFONT)
        close = pyogi.Frame(label)
        close.bind_event_mode(MOUSEBUTTONUP, 1, pyogi.dialog.clean)
        label.bind_event_mode(MOUSEBUTTONUP, 1, pyogi.dialog.clean)
        label = pyogi.Label("free", DEFAULTFONT)
        self.free = pyogi.Frame(label)
        self.free.bind_event_mode(MOUSEBUTTONUP, 1, lambda turret=turret,
                                                           container=container:
                                  self.demontiere(turret, container, simbol))
        label.bind_event_mode(MOUSEBUTTONUP, 1, lambda turret=turret,
                                                       container=container:
                              self.demontiere(turret, container, simbol))
        container.add(close)
        container.add_all(waffen)
        container.add(self.free)
        waffen_auswahl = pyogi.Frame(container)
        pyogi.dialog.set(waffen_auswahl, simbol._master.global_side("midleft"),
                         "midright")
        self.set_mark(turret["pos"])
