
import random
import os
import math

import pygame

from tools import open_yaml
from geschuetze import Geschuetz
import move
import image
import effects
import objects
import items
import pyplay


class Feind(pygame.sprite.Sprite, move.MoveBase):
    """Feindliche Schiffe die nach einen vorgegebenen muster agieren."""

    def __init__(self, game, gruppen, dater):
        """Feindliche Schiffe die nach einen vorgegebenen muster agieren."""
        pygame.sprite.Sprite.__init__(self, *gruppen)
        move.MoveBase.__init__(self, (0, 0), 0, dater)

        self.game = game
        self._energie = dater["energie"]
        self.movement = move.moves[dater["movement"]]
        self.org_image = image.get(os.path.join("daten", "Feinde", "Images",
                                                dater["image"]))
        self.image = self.org_image
        self.explosion_image = image.get(
            os.path.join("daten", "effects", "Images",
                         dater["explosion_image"]))
        self.sound_explusion = os.path.join("daten", "Sound",
                                            dater["sound_explusion"])
        self.rect = self.image.get_rect()
        self.ziel = game.player.ship
        self.geschuetze = []
        for geschuetz_dater in dater["geschütze"]:
            geschuetz = Geschuetz(geschuetz_dater)
            self.geschuetze.append(geschuetz)
        self.killed = False
        self.explosion = dater["explosion"]
        self.loot = dater["items"]

    def update(self, past_time):
        if not self.killed:
            for geschuetz in self.geschuetze:
                geschuetz.update(past_time)
            if self.game.live_field.contains(self.rect):
                self.movement(self, past_time)
                self.feuer()
            else:
                self.kill()
        else:
            effects.Effect((self.game.alle,), self.rect.center,
                           self.explosion_image, 100)
            if self.explosion:
                for tile in self.explosion:
                    teil = open_yaml(os.path.join("daten", "truemmer", tile))
                    radians = math.radians(random.randrange(0, 360))
                    teil["speed"] += random.randrange(200)/1000
                    drum = objects.Truemmer(self.game, (self.game.alle,
                                                        self.game.truemmer),
                                            teil)
                    drum.set_pos(self.rect.center, "center")
                    drum.set_rotation(radians)
            if self.loot:
                for item in self.loot:
                    chance = self.loot[item]
                    if chance > 1 and random.randrange(chance) != 1:
                        continue
                    item = open_yaml(os.path.join("daten", "items", item))
                    item = items.Item(self.game, (self.game.alle,
                                                  self.game.geld
                                                  if item["typ"] == "Geld"
                                                  else self.game.energie), item
                                      )
                    item.set_pos(self.rect.center, "center")
            self.kill()
            pyplay.play(self.sound_explusion)


    @property
    def energie(self):
        return self._energie

    @energie.setter
    def energie(self,  value):
        self._energie = value
        if self._energie < 1:
            self.killed = True

    def feuer(self):
        """Feuert alle Geschuetze des Feindes ab."""
        for geschuetz in self.geschuetze:
            geschuetz.feuer((self.game.feind_geschosse, self.game.alle),
                            self.pos, None, ziel=None, game=self.game)
