"""Funktionen oder Classen zur animaton fon sprites."""

import pygame


class linear:
    """Gradliniger Durchlauf der sprite animation."""

    def __init__(self, sprite, image, dater):
        """Gradliniger Durchlauf der sprite animation.

        dater:{w, h,
               delay:zeit bis zum naechsten Bild,
               forward:ob die Animation
               vorwaerts oder ruekwerts abgespielt wird}.
        """
        self.image = image
        self.sprite = sprite
        sprite.animated = True
        sprite.update = self
        self.w = dater["w"]
        self.h = dater["h"]
        self.delay = dater["delay"]
        # self.items = dater["items"]
        self.forward = dater.get("forward", True)
        self.x = 0
        self.y = 0
        image_size = self.image.get_size()
        self.item_w = image_size[0] // self.w - 1
        self.item_h = image_size[1] // self.h - 1
        self.time = 0
        self.sprite.image = self.image.subsurface(0, 0, self.w, self.h)

    def update(self, past_time):
        self.time += past_time
        if self.time >= self.delay:
            self.time = 0
            if self.forward:
                self.x += 1
                if self.x > self.item_w:
                    self.x = 0
                    self.y = self.y + 1 if self.y < self.item_h else 0
            else:
                self.x -= 1
                if self.x < 0:
                    self.x = self.item_w
                    self.y = self.y - 1 if self.y > 0 else self.item_h
            if self.sprite.rotate:
                self.sprite.image = pygame.transform.rotate(
                    self.image.subsurface(self.x * self.w, self.y * self.h,
                                          self.w, self.h),
                    -self.sprite.pos.degrees)
            else:
                self.sprite.image = self.image.subsurface(self.x * self.w,
                                                          self.y * self.h,
                                                          self.w, self.h)

    def __call__(self, past_time):
        self.update(past_time)
        self.sprite.__class__.update(self.sprite, past_time)  # ????????
