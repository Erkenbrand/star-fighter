"""audio player (not for musik)"""

import pygame

audiofiles = {}


def play(path):
    try:
        audiofiles[path].play()
    except KeyError:
        audio = pygame.mixer.Sound(path)
        audiofiles[path] = audio
        audio.play()
